# Group 6 Final Report

[toc]

## Team Members

**Haijian Wang**
- I am a M.S student in ECE at the University of Illinois at Urbana-Champaign. I mainly focus on computer architecture, AI/ML, and HW/SW codesign. I also obtained a physics minor with emphasise on quantum.
- My email is haijian4@illinois.edu. Don't hesitate to email me if you have any questions!

**Jack Prokop (jprokop2@illinois.edu)**
- I am a current undergraduate student studying Computer Engineering with a graduation date in May 2025. I have taken courses in systems programming, parallel programming, digital systems, and algorithms. I am interested in embedded software and have skills including C/C++, CUDA, SystemVerilog, and Python.
- My email is jprokop2@illinois.edu


**Sudheep Iyer**
- I am a current ECE major undergrad student with a minor in semiconductor engineering. I have both software and hardware experience with C/C++, Python, FPGAs, and PCB design.
- My Illinois email is sudheep2@illinois.edu.

## Introduction:

This repository consists of two parts: the first part is a hardware IP for offloading checksum, and the second part is an attempt to decode the DEEP protocol in hardware. 

Both hardware IPs are implemented on the AMD Pynq Z2 FPGA. The checksum is a value calculated within the received packet that is computed and then compared with the checksum value sent along with the transmitted packet. Comparing these values provides insight into whether there was any data loss in the transmitted packets. Typically checksums are calculated by the CPU, but we offload this into our PL checksum IP. By offloading the checksum calculation from the CPU, we can maximize CPU utilization to perform other tasks.

DEEP is a format of data that the IEX use to broadcast market data. We designed our hardware based on the parsing script provided by this class and the official manual. At the moment, we only support type 5 and type 8 which are for buy/sell price levels updates. We started the design with this type because it’s the most essential one for trading. 

**FPGA Board: Pynq z2**

This board incorporates both FPGA architecture and a 650MHz dual-core Cortex-A9 processor that can be added to a block design using the Zync Processing System IP.
The FPGA portion of the board is comprised of 13,300 logic slices, each with four 6-input LUTs. Most importantly for our project, there is a peripheral 1Gb Ethernet Port and Controller.

**Setup/Tutorials to Begin**

The xpr file is the project file for Vivado. Open the project file with vivado, and rest of the files will be loaded according to their local relative path. Our project used different levels of tools and designs. The main architecture of the system is laid out in the block design. Our customized hardware	IP is integrated into the overall system through this tool. For the checksum example, we read through the AXI streaming protocol and wrote the logics that can communicate with the DMA controller through this protocol. For the DEEP decoder, because the AXI protocol is way too complicated, we utilized the HLS tool provided by Xilinx so that all the communication between signals are handled by the tool. After building and implementing the hardware architecture, we used the Xilinx SDK to build out our software. For this project, we are only running bare-metal C program on the included Arm core on the board. Technically, you could also run Linux on this board. 

The Xilinx SDK will auto-generate a package of drivers (called BSP) for all the hardware in the system. Additionally, example code for some Xilinx-provided IPs can be imported to help you get started. In our case, we used the XeMacPS example to receive and transmit ethernet frames. The communication model is utilizing BDs (buffer descriptor) and interrupts. The links for the tutorials we studied to understand the implementation of this ethernet driver are listed in the external links section.


**Block Design/Architecture**
![Simple block diagram ](Block_diagram.jpg)




## Checksum IP:

**Checksum Calculation**

The TCP checksum is calculated using the ones complement addition of all 16 bit words in the TCP pseudo header and TCP Segment. The Pseudo Header includes the Source Address, Destination Address, Reserved + Protocol, and Segement Length. Ones complement addition is performed by adding each consecutive 16 bit word together, adding the overflow bit, and at the end, inverting all of the bits.

**AXI4 Streaming Protocol** 

The AXI protocol is used for on-chip communication on the Pynq z2 with read/write data channels. In Vivado’s IP integrator, we were able to include the AXI DMA IP to handle data transfer between the DRAM and custom checksum offloader. 

The AXI4 Streaming Interface is used to connect the Checksum IP Module to the incoming data frame as well as the DMA. On the incoming, s, side of the AXI4 stream we have the signals s_axis_tdata, s_axis_tkeep, s_axis_tlast, s_axis_tvalid, s_axis_tready and a matching set on the outgoing, m, side. The tvalid signal is used to indicate valid data on the tdata line, the tready signal is used to indicate that the receiver is ready for the next set of data, and the tlast signal is used to indicate the last set of data. This protocol is also little endian.

**IP Functionality** 

This module makes use of a 16-bit AXI4 Stream connection, and must transform the endianness of the protocol. Until the s_axis_tlast signal is asserted, the sum of the incoming 2-bytes and the previous bytes are accumulated, and the overflow bit is added. Once the s_axis_tlast signal is asserted, the m_axis_tvalid and m_axis_tlast signals are asserted to indicate that the final checksum will be valid on the output, and the m_axis_tdata will contain the inverted accumulation. After the final checksum is sent out, the m_axis_tvalid, m_axis_tdata, and m_axis_tlast signals are set low.

## DEEP IP:

**DEEP Protocol**

The Price Level Update Message is a crucial component in financial markets, providing information about changes in price levels for various securities. These messages play a vital role in enabling traders and investors to make informed and timely decisions in the dynamic world of financial markets. Our hardware design is based on the parsing script provided by the course. The specification of the type 5 message is shown below.

![Simple block diagram ](deep1.PNG)

**HLS**

Our hardware design for this module is done by High Level Synthesis. We followed the official vivado manual and other online tutorials to get us started with this tool. A small tutorial video is provided by us in this project as well. In general, HLS is basically using high-level programming language such as C to describe hardware. The key concept in HLS is pragmas. It's basically telling the compiler how you want the specified part of your module to behave. In our case, we used pragmas to specify the input and output port of our module to be compatible with AXI protocols. The HLS compiler will automatically generate all the control signals for the hardware module. 

![Simple block diagram ](hls.png)

## External Links for tutorials:
https://igorfreire.com.br/2016/12/28/understanding-gigabit-ethernet-controllers-dma-zynq-devices/

https://igorfreire.com.br/2016/11/19/zynq-ethernet-interface-zybo-board/


## Team contributions:

**Haijian:**

Block and architecture design of both projects. Figuring out how to use ILA, Ethernet PHY/MAC, Xemacps driver, DMA driver, HLS, and the DEEP decoder. Combine everything and build up the whole project. Record tutorials.

**Sudheep:**

Learning about AXI protocols. Making AXI4-Stream interface with INTERFACE directive. Debugging AXI interface with Vivado IP Integrator. Integrating AXI interface with Checksum IP.

**Jack:**

Learned about checksum calculation, TCP Checksum IP, peripheral connections, debugging
