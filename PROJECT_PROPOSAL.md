# Current Ideas:
https://github.com/alexforencich/verilog-ethernet

Our current idea would be to build a hardware debugging platform that can feed real Ethernet data to another hardware IP design or software. Another one would be building a PHY chip with the help or some open source one. One more would be offloading some tasks to a customized hardware module using DMA calls and AXI protocol from CPU and send back.  

Our main language would be Verilog and System Verilog. It will be used for our hardware design and testbench. High level synthesis and CHISEL are becoming popular for IP design, and we intend to use those as well, though it might take some significant time to learn. We will also need C/C++ and Python if we intend to use some form of software to process Ethernet data. Vivado has its own software libraries support for its boards, and we could potentially benefit from it. We will mainly use AMD FPGA boards (Pynq z2 and VCU 118). We will also need desktop Linux and Windows system for programming. Vivado would also be required to program FPGAs.

In terms of user interface, we have two concepts in mind. If we decide to go for high level synthesis and CHISEL, we can create a tutorial for programing with these languages. For our actual Verilog implementation, we could make it configurable and build a user guide for it. We won’t involve any UI designs such as web pages.

# Frameworks/Libraries:

For version control we plan to use git by taking advantage of the group gitlab repo for our project. This would be used to manage our Verilog/System Verilog code and for documentation on our code as well as where guides for building and running our project would live. For hardware design verification and testing, we can use simulators such as Vivado’s built-in simulator or ModelSim. Using AMD FPGA boards, we will use Vivado Design Suite for programming, and may use Vivado high-level synthesis or CHISEL for higher user accessibility. Within Vivado, MicroBlaze and AXI IP cores will likely be used.

# Timeline:
With about 7 weeks for our project, the following is an estimated timeline:
Week 1: Obtain approval for project proposal, clarify any questions and gain access to all required materials and resources. In addition, complete VM setup for Vivado/Vitis.
Week 2: Begin Verilog/SystemVerilog for hardware debugging platform
Week 3: Continue with Verilog/SystemVerilog deign and apply HLS or CHISEL to aid in IP design
Week 4: Complete testbenches for simulation and begin testing and modifying design as needed, learn about AXI protocol for data transfer between hardware modules.
Week 5: Complete and test hardware IP and test with physical ethernet, develop any software components required for testing.
Week 6: Debugging, updating documentation, and continue testing.
Week 7: Complete any user guides and documentation, develop tutorials for high-level synthesis and CHISEL, turn in final project.  


# Bare Minimum goals:

At the very least we expect to develop a functional debugging platform that can take in real raw Ethernet input, process it, and output simulated signals that represent sending the data to another device.

# Expected Completion:

Our target goal is to develop a debugging platform that can receive real ethernet packets and then process and transmit the data to CPU or another device via AXI protocol.

# Reach:

Our reach goal would be to create more of the components such as the PHY chip, for ethernet communication, or to extend the application of our project by including other hardware devices such as our Raspberry Pis to transmit and receive packet data utilizing hardware acceleration.

# Team-Member Specific Contributions:

	Potential Project Breakdown:
•	Testing/Testbench Design
•	Script to send network data from a computer
•	Ethernet Controller (PHY Chip?)
•	AXI Interface Integration
•	FSM Modules for Packet Decoding
•	HW Modules to Send/Receive Packets
•	Other Integration of IP modules
•	Drivers?
•	VM Setup

An initial division of responsibilities is as follows:
All team members will setup VM for Vivado and acquire all necessary libraries and frameworks to ensure all members can work on all aspects of the project.
Sudheep will work on testbenches, testing, and script to send network data from a computer.
Haijian will work on using or developing PHY chip for ethernet, as well as figure out AXI protocol to transmit data to other components.
Jack will work on the FSM modules for decoding packet data as well as the hardware modules for transmitting and receiving packet data.
# Note: This is a preliminary division of tasks and will most likely be subject to change. 

# External Resources:

FPGA Board, Raspberry Pis, Ethernet Equipment, IP modules within Vivado, potentially using previous IE421 Projects and open source projects online. We will need guidance about how to best use the FPGA to detect/decode specific network packets and what to do with the data from there. More specifically, FSM design to interpret packet + process specific packet fields depending on the type of packet received or even overall system design. Also, additional explanations of networking would be valuable as we all have limited working knowledge about the topic.

# Final Deliverables:

	Target: Likely a combination of console output or testbench waveforms. Comparison of latency statistics between HW and SW decoding of network packets. 

	Reach: Real-Time terminal outputs of packets sent and received between two RBPIs imitating Firm and Exchange Computers using the HW acceleration on the Firm Computer end.

# Member Description:
Haijian Wang: Expect to graduation with M.S. in ECE in May 2025. Intend to find a job in IC verification/design. Interested in HFT firms after taking this class as well. 

Sudheep Iyer: Expected Graduation B.S. in ECE December 2024. Plan on applying to grad school afterwards at UIUC.

Jack Prokop: Expected Graduation B.S May 2025. Intending to enter industry in either Embedded Software Engineering or other low-level software roles.