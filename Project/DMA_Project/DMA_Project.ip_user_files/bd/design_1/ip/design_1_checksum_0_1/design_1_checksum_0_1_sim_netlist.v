// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Sep 23 19:24:27 2023
// Host        : DESKTOP-IDF6QBU running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               D:/xilinx/Ethernet_Starter_Code_v2_just_need_to_update_checksum_ipv4noUDPok/DMA_Project/DMA_Project.srcs/sources_1/bd/design_1/ip/design_1_checksum_0_1/design_1_checksum_0_1_sim_netlist.v
// Design      : design_1_checksum_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_checksum_0_1,checksum,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "checksum,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_checksum_0_1
   (m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast,
    m_axis_tready,
    m_axis_tvalid,
    s_axis_aclk,
    s_axis_aresetn,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    s_axis_tready,
    s_axis_tvalid);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) output [15:0]m_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TKEEP" *) output [1:0]m_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) output m_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) input m_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0" *) output m_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis_aclk, ASSOCIATED_BUSIF s_axis, ASSOCIATED_RESET s_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_2_FCLK_CLK0, INSERT_VIP 0" *) input s_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s_axis_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) input [15:0]s_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TKEEP" *) input [1:0]s_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) input s_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) output s_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_2_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s_axis_tvalid;

  wire \<const1> ;
  wire [15:0]m_axis_tdata;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire s_axis_aclk;
  wire s_axis_aresetn;
  wire [15:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;

  assign m_axis_tkeep[1] = \<const1> ;
  assign m_axis_tkeep[0] = \<const1> ;
  assign m_axis_tvalid = m_axis_tlast;
  VCC VCC
       (.P(\<const1> ));
  design_1_checksum_0_1_checksum inst
       (.m_axis_tdata(m_axis_tdata),
        .m_axis_tready(m_axis_tready),
        .m_axis_tvalid_reg_0(m_axis_tlast),
        .s_axis_aclk(s_axis_aclk),
        .s_axis_aresetn(s_axis_aresetn),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tvalid(s_axis_tvalid));
endmodule

(* ORIG_REF_NAME = "checksum" *) 
module design_1_checksum_0_1_checksum
   (m_axis_tvalid_reg_0,
    m_axis_tdata,
    s_axis_tready,
    s_axis_tlast,
    s_axis_tvalid,
    m_axis_tready,
    s_axis_aresetn,
    s_axis_aclk,
    s_axis_tdata);
  output m_axis_tvalid_reg_0;
  output [15:0]m_axis_tdata;
  output s_axis_tready;
  input s_axis_tlast;
  input s_axis_tvalid;
  input m_axis_tready;
  input s_axis_aresetn;
  input s_axis_aclk;
  input [15:0]s_axis_tdata;

  wire [15:0]m_axis_tdata;
  wire \m_axis_tdata[11]_i_10_n_0 ;
  wire \m_axis_tdata[11]_i_11_n_0 ;
  wire \m_axis_tdata[11]_i_2_n_0 ;
  wire \m_axis_tdata[11]_i_3_n_0 ;
  wire \m_axis_tdata[11]_i_4_n_0 ;
  wire \m_axis_tdata[11]_i_5_n_0 ;
  wire \m_axis_tdata[11]_i_8_n_0 ;
  wire \m_axis_tdata[11]_i_9_n_0 ;
  wire \m_axis_tdata[15]_i_10_n_0 ;
  wire \m_axis_tdata[15]_i_11_n_0 ;
  wire \m_axis_tdata[15]_i_12_n_0 ;
  wire \m_axis_tdata[15]_i_13_n_0 ;
  wire \m_axis_tdata[15]_i_1_n_0 ;
  wire \m_axis_tdata[15]_i_2_n_0 ;
  wire \m_axis_tdata[15]_i_4_n_0 ;
  wire \m_axis_tdata[15]_i_5_n_0 ;
  wire \m_axis_tdata[15]_i_6_n_0 ;
  wire \m_axis_tdata[15]_i_7_n_0 ;
  wire \m_axis_tdata[3]_i_10_n_0 ;
  wire \m_axis_tdata[3]_i_11_n_0 ;
  wire \m_axis_tdata[3]_i_12_n_0 ;
  wire \m_axis_tdata[3]_i_13_n_0 ;
  wire \m_axis_tdata[3]_i_14_n_0 ;
  wire \m_axis_tdata[3]_i_2_n_0 ;
  wire \m_axis_tdata[3]_i_3_n_0 ;
  wire \m_axis_tdata[3]_i_4_n_0 ;
  wire \m_axis_tdata[3]_i_5_n_0 ;
  wire \m_axis_tdata[3]_i_6_n_0 ;
  wire \m_axis_tdata[7]_i_10_n_0 ;
  wire \m_axis_tdata[7]_i_11_n_0 ;
  wire \m_axis_tdata[7]_i_2_n_0 ;
  wire \m_axis_tdata[7]_i_3_n_0 ;
  wire \m_axis_tdata[7]_i_4_n_0 ;
  wire \m_axis_tdata[7]_i_5_n_0 ;
  wire \m_axis_tdata[7]_i_8_n_0 ;
  wire \m_axis_tdata[7]_i_9_n_0 ;
  wire \m_axis_tdata_reg[11]_i_1_n_0 ;
  wire \m_axis_tdata_reg[11]_i_1_n_1 ;
  wire \m_axis_tdata_reg[11]_i_1_n_2 ;
  wire \m_axis_tdata_reg[11]_i_1_n_3 ;
  wire \m_axis_tdata_reg[11]_i_1_n_4 ;
  wire \m_axis_tdata_reg[11]_i_1_n_5 ;
  wire \m_axis_tdata_reg[11]_i_1_n_6 ;
  wire \m_axis_tdata_reg[11]_i_1_n_7 ;
  wire \m_axis_tdata_reg[11]_i_6_n_0 ;
  wire \m_axis_tdata_reg[11]_i_6_n_1 ;
  wire \m_axis_tdata_reg[11]_i_6_n_2 ;
  wire \m_axis_tdata_reg[11]_i_6_n_3 ;
  wire \m_axis_tdata_reg[11]_i_7_n_0 ;
  wire \m_axis_tdata_reg[11]_i_7_n_1 ;
  wire \m_axis_tdata_reg[11]_i_7_n_2 ;
  wire \m_axis_tdata_reg[11]_i_7_n_3 ;
  wire \m_axis_tdata_reg[11]_i_7_n_4 ;
  wire \m_axis_tdata_reg[11]_i_7_n_5 ;
  wire \m_axis_tdata_reg[11]_i_7_n_6 ;
  wire \m_axis_tdata_reg[11]_i_7_n_7 ;
  wire \m_axis_tdata_reg[15]_i_3_n_1 ;
  wire \m_axis_tdata_reg[15]_i_3_n_2 ;
  wire \m_axis_tdata_reg[15]_i_3_n_3 ;
  wire \m_axis_tdata_reg[15]_i_3_n_4 ;
  wire \m_axis_tdata_reg[15]_i_3_n_5 ;
  wire \m_axis_tdata_reg[15]_i_3_n_6 ;
  wire \m_axis_tdata_reg[15]_i_3_n_7 ;
  wire \m_axis_tdata_reg[15]_i_8_n_1 ;
  wire \m_axis_tdata_reg[15]_i_8_n_2 ;
  wire \m_axis_tdata_reg[15]_i_8_n_3 ;
  wire \m_axis_tdata_reg[15]_i_9_n_0 ;
  wire \m_axis_tdata_reg[15]_i_9_n_1 ;
  wire \m_axis_tdata_reg[15]_i_9_n_2 ;
  wire \m_axis_tdata_reg[15]_i_9_n_3 ;
  wire \m_axis_tdata_reg[15]_i_9_n_4 ;
  wire \m_axis_tdata_reg[15]_i_9_n_5 ;
  wire \m_axis_tdata_reg[15]_i_9_n_6 ;
  wire \m_axis_tdata_reg[15]_i_9_n_7 ;
  wire \m_axis_tdata_reg[3]_i_1_n_0 ;
  wire \m_axis_tdata_reg[3]_i_1_n_1 ;
  wire \m_axis_tdata_reg[3]_i_1_n_2 ;
  wire \m_axis_tdata_reg[3]_i_1_n_3 ;
  wire \m_axis_tdata_reg[3]_i_1_n_4 ;
  wire \m_axis_tdata_reg[3]_i_1_n_5 ;
  wire \m_axis_tdata_reg[3]_i_1_n_6 ;
  wire \m_axis_tdata_reg[3]_i_1_n_7 ;
  wire \m_axis_tdata_reg[3]_i_8_n_0 ;
  wire \m_axis_tdata_reg[3]_i_8_n_1 ;
  wire \m_axis_tdata_reg[3]_i_8_n_2 ;
  wire \m_axis_tdata_reg[3]_i_8_n_3 ;
  wire \m_axis_tdata_reg[3]_i_9_n_0 ;
  wire \m_axis_tdata_reg[3]_i_9_n_1 ;
  wire \m_axis_tdata_reg[3]_i_9_n_2 ;
  wire \m_axis_tdata_reg[3]_i_9_n_3 ;
  wire \m_axis_tdata_reg[3]_i_9_n_4 ;
  wire \m_axis_tdata_reg[3]_i_9_n_5 ;
  wire \m_axis_tdata_reg[3]_i_9_n_6 ;
  wire \m_axis_tdata_reg[3]_i_9_n_7 ;
  wire \m_axis_tdata_reg[7]_i_1_n_0 ;
  wire \m_axis_tdata_reg[7]_i_1_n_1 ;
  wire \m_axis_tdata_reg[7]_i_1_n_2 ;
  wire \m_axis_tdata_reg[7]_i_1_n_3 ;
  wire \m_axis_tdata_reg[7]_i_1_n_4 ;
  wire \m_axis_tdata_reg[7]_i_1_n_5 ;
  wire \m_axis_tdata_reg[7]_i_1_n_6 ;
  wire \m_axis_tdata_reg[7]_i_1_n_7 ;
  wire \m_axis_tdata_reg[7]_i_6_n_0 ;
  wire \m_axis_tdata_reg[7]_i_6_n_1 ;
  wire \m_axis_tdata_reg[7]_i_6_n_2 ;
  wire \m_axis_tdata_reg[7]_i_6_n_3 ;
  wire \m_axis_tdata_reg[7]_i_7_n_0 ;
  wire \m_axis_tdata_reg[7]_i_7_n_1 ;
  wire \m_axis_tdata_reg[7]_i_7_n_2 ;
  wire \m_axis_tdata_reg[7]_i_7_n_3 ;
  wire \m_axis_tdata_reg[7]_i_7_n_4 ;
  wire \m_axis_tdata_reg[7]_i_7_n_5 ;
  wire \m_axis_tdata_reg[7]_i_7_n_6 ;
  wire \m_axis_tdata_reg[7]_i_7_n_7 ;
  wire m_axis_tready;
  wire m_axis_tvalid_i_1_n_0;
  wire m_axis_tvalid_reg_0;
  wire p_0_in;
  wire s_axis_aclk;
  wire s_axis_aresetn;
  wire [15:0]s_axis_tdata;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire s_axis_tvalid;
  wire [15:0]temp_m_axis_tdata;
  wire [3:3]\NLW_m_axis_tdata_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_m_axis_tdata_reg[15]_i_8_CO_UNCONNECTED ;
  wire [3:1]\NLW_m_axis_tdata_reg[3]_i_7_CO_UNCONNECTED ;
  wire [3:0]\NLW_m_axis_tdata_reg[3]_i_7_O_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[11]_i_10 
       (.I0(s_axis_tdata[1]),
        .I1(m_axis_tdata[9]),
        .O(\m_axis_tdata[11]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[11]_i_11 
       (.I0(s_axis_tdata[0]),
        .I1(m_axis_tdata[8]),
        .O(\m_axis_tdata[11]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[11]_i_2 
       (.I0(temp_m_axis_tdata[11]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[11]_i_7_n_4 ),
        .O(\m_axis_tdata[11]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[11]_i_3 
       (.I0(temp_m_axis_tdata[10]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[11]_i_7_n_5 ),
        .O(\m_axis_tdata[11]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[11]_i_4 
       (.I0(temp_m_axis_tdata[9]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[11]_i_7_n_6 ),
        .O(\m_axis_tdata[11]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[11]_i_5 
       (.I0(temp_m_axis_tdata[8]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[11]_i_7_n_7 ),
        .O(\m_axis_tdata[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[11]_i_8 
       (.I0(s_axis_tdata[3]),
        .I1(m_axis_tdata[11]),
        .O(\m_axis_tdata[11]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[11]_i_9 
       (.I0(s_axis_tdata[2]),
        .I1(m_axis_tdata[10]),
        .O(\m_axis_tdata[11]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h8F)) 
    \m_axis_tdata[15]_i_1 
       (.I0(m_axis_tvalid_reg_0),
        .I1(m_axis_tready),
        .I2(s_axis_aresetn),
        .O(\m_axis_tdata[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[15]_i_10 
       (.I0(s_axis_tdata[7]),
        .I1(m_axis_tdata[15]),
        .O(\m_axis_tdata[15]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[15]_i_11 
       (.I0(s_axis_tdata[6]),
        .I1(m_axis_tdata[14]),
        .O(\m_axis_tdata[15]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[15]_i_12 
       (.I0(s_axis_tdata[5]),
        .I1(m_axis_tdata[13]),
        .O(\m_axis_tdata[15]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[15]_i_13 
       (.I0(s_axis_tdata[4]),
        .I1(m_axis_tdata[12]),
        .O(\m_axis_tdata[15]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \m_axis_tdata[15]_i_2 
       (.I0(s_axis_tvalid),
        .I1(m_axis_tvalid_reg_0),
        .O(\m_axis_tdata[15]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[15]_i_4 
       (.I0(temp_m_axis_tdata[15]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[15]_i_9_n_4 ),
        .O(\m_axis_tdata[15]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[15]_i_5 
       (.I0(temp_m_axis_tdata[14]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[15]_i_9_n_5 ),
        .O(\m_axis_tdata[15]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[15]_i_6 
       (.I0(temp_m_axis_tdata[13]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[15]_i_9_n_6 ),
        .O(\m_axis_tdata[15]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[15]_i_7 
       (.I0(temp_m_axis_tdata[12]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[15]_i_9_n_7 ),
        .O(\m_axis_tdata[15]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[3]_i_10 
       (.I0(\m_axis_tdata_reg[3]_i_9_n_7 ),
        .I1(p_0_in),
        .O(\m_axis_tdata[3]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[3]_i_11 
       (.I0(s_axis_tdata[11]),
        .I1(m_axis_tdata[3]),
        .O(\m_axis_tdata[3]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[3]_i_12 
       (.I0(s_axis_tdata[10]),
        .I1(m_axis_tdata[2]),
        .O(\m_axis_tdata[3]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[3]_i_13 
       (.I0(s_axis_tdata[9]),
        .I1(m_axis_tdata[1]),
        .O(\m_axis_tdata[3]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[3]_i_14 
       (.I0(s_axis_tdata[8]),
        .I1(m_axis_tdata[0]),
        .O(\m_axis_tdata[3]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \m_axis_tdata[3]_i_2 
       (.I0(p_0_in),
        .I1(s_axis_tlast),
        .O(\m_axis_tdata[3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[3]_i_3 
       (.I0(temp_m_axis_tdata[3]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[3]_i_9_n_4 ),
        .O(\m_axis_tdata[3]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[3]_i_4 
       (.I0(temp_m_axis_tdata[2]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[3]_i_9_n_5 ),
        .O(\m_axis_tdata[3]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[3]_i_5 
       (.I0(temp_m_axis_tdata[1]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[3]_i_9_n_6 ),
        .O(\m_axis_tdata[3]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[3]_i_6 
       (.I0(temp_m_axis_tdata[0]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[3]_i_9_n_7 ),
        .O(\m_axis_tdata[3]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[7]_i_10 
       (.I0(s_axis_tdata[13]),
        .I1(m_axis_tdata[5]),
        .O(\m_axis_tdata[7]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[7]_i_11 
       (.I0(s_axis_tdata[12]),
        .I1(m_axis_tdata[4]),
        .O(\m_axis_tdata[7]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[7]_i_2 
       (.I0(temp_m_axis_tdata[7]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[7]_i_7_n_4 ),
        .O(\m_axis_tdata[7]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[7]_i_3 
       (.I0(temp_m_axis_tdata[6]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[7]_i_7_n_5 ),
        .O(\m_axis_tdata[7]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[7]_i_4 
       (.I0(temp_m_axis_tdata[5]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[7]_i_7_n_6 ),
        .O(\m_axis_tdata[7]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h74)) 
    \m_axis_tdata[7]_i_5 
       (.I0(temp_m_axis_tdata[4]),
        .I1(s_axis_tlast),
        .I2(\m_axis_tdata_reg[7]_i_7_n_7 ),
        .O(\m_axis_tdata[7]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[7]_i_8 
       (.I0(s_axis_tdata[15]),
        .I1(m_axis_tdata[7]),
        .O(\m_axis_tdata[7]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \m_axis_tdata[7]_i_9 
       (.I0(s_axis_tdata[14]),
        .I1(m_axis_tdata[6]),
        .O(\m_axis_tdata[7]_i_9_n_0 ));
  FDRE \m_axis_tdata_reg[0] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[3]_i_1_n_7 ),
        .Q(m_axis_tdata[0]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[10] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[11]_i_1_n_5 ),
        .Q(m_axis_tdata[10]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[11] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[11]_i_1_n_4 ),
        .Q(m_axis_tdata[11]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  CARRY4 \m_axis_tdata_reg[11]_i_1 
       (.CI(\m_axis_tdata_reg[7]_i_1_n_0 ),
        .CO({\m_axis_tdata_reg[11]_i_1_n_0 ,\m_axis_tdata_reg[11]_i_1_n_1 ,\m_axis_tdata_reg[11]_i_1_n_2 ,\m_axis_tdata_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\m_axis_tdata_reg[11]_i_1_n_4 ,\m_axis_tdata_reg[11]_i_1_n_5 ,\m_axis_tdata_reg[11]_i_1_n_6 ,\m_axis_tdata_reg[11]_i_1_n_7 }),
        .S({\m_axis_tdata[11]_i_2_n_0 ,\m_axis_tdata[11]_i_3_n_0 ,\m_axis_tdata[11]_i_4_n_0 ,\m_axis_tdata[11]_i_5_n_0 }));
  CARRY4 \m_axis_tdata_reg[11]_i_6 
       (.CI(\m_axis_tdata_reg[7]_i_6_n_0 ),
        .CO({\m_axis_tdata_reg[11]_i_6_n_0 ,\m_axis_tdata_reg[11]_i_6_n_1 ,\m_axis_tdata_reg[11]_i_6_n_2 ,\m_axis_tdata_reg[11]_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(temp_m_axis_tdata[11:8]),
        .S({\m_axis_tdata_reg[11]_i_7_n_4 ,\m_axis_tdata_reg[11]_i_7_n_5 ,\m_axis_tdata_reg[11]_i_7_n_6 ,\m_axis_tdata_reg[11]_i_7_n_7 }));
  CARRY4 \m_axis_tdata_reg[11]_i_7 
       (.CI(\m_axis_tdata_reg[7]_i_7_n_0 ),
        .CO({\m_axis_tdata_reg[11]_i_7_n_0 ,\m_axis_tdata_reg[11]_i_7_n_1 ,\m_axis_tdata_reg[11]_i_7_n_2 ,\m_axis_tdata_reg[11]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[3:0]),
        .O({\m_axis_tdata_reg[11]_i_7_n_4 ,\m_axis_tdata_reg[11]_i_7_n_5 ,\m_axis_tdata_reg[11]_i_7_n_6 ,\m_axis_tdata_reg[11]_i_7_n_7 }),
        .S({\m_axis_tdata[11]_i_8_n_0 ,\m_axis_tdata[11]_i_9_n_0 ,\m_axis_tdata[11]_i_10_n_0 ,\m_axis_tdata[11]_i_11_n_0 }));
  FDRE \m_axis_tdata_reg[12] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[15]_i_3_n_7 ),
        .Q(m_axis_tdata[12]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[13] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[15]_i_3_n_6 ),
        .Q(m_axis_tdata[13]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[14] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[15]_i_3_n_5 ),
        .Q(m_axis_tdata[14]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[15] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[15]_i_3_n_4 ),
        .Q(m_axis_tdata[15]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  CARRY4 \m_axis_tdata_reg[15]_i_3 
       (.CI(\m_axis_tdata_reg[11]_i_1_n_0 ),
        .CO({\NLW_m_axis_tdata_reg[15]_i_3_CO_UNCONNECTED [3],\m_axis_tdata_reg[15]_i_3_n_1 ,\m_axis_tdata_reg[15]_i_3_n_2 ,\m_axis_tdata_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\m_axis_tdata_reg[15]_i_3_n_4 ,\m_axis_tdata_reg[15]_i_3_n_5 ,\m_axis_tdata_reg[15]_i_3_n_6 ,\m_axis_tdata_reg[15]_i_3_n_7 }),
        .S({\m_axis_tdata[15]_i_4_n_0 ,\m_axis_tdata[15]_i_5_n_0 ,\m_axis_tdata[15]_i_6_n_0 ,\m_axis_tdata[15]_i_7_n_0 }));
  CARRY4 \m_axis_tdata_reg[15]_i_8 
       (.CI(\m_axis_tdata_reg[11]_i_6_n_0 ),
        .CO({\NLW_m_axis_tdata_reg[15]_i_8_CO_UNCONNECTED [3],\m_axis_tdata_reg[15]_i_8_n_1 ,\m_axis_tdata_reg[15]_i_8_n_2 ,\m_axis_tdata_reg[15]_i_8_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(temp_m_axis_tdata[15:12]),
        .S({\m_axis_tdata_reg[15]_i_9_n_4 ,\m_axis_tdata_reg[15]_i_9_n_5 ,\m_axis_tdata_reg[15]_i_9_n_6 ,\m_axis_tdata_reg[15]_i_9_n_7 }));
  CARRY4 \m_axis_tdata_reg[15]_i_9 
       (.CI(\m_axis_tdata_reg[11]_i_7_n_0 ),
        .CO({\m_axis_tdata_reg[15]_i_9_n_0 ,\m_axis_tdata_reg[15]_i_9_n_1 ,\m_axis_tdata_reg[15]_i_9_n_2 ,\m_axis_tdata_reg[15]_i_9_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[7:4]),
        .O({\m_axis_tdata_reg[15]_i_9_n_4 ,\m_axis_tdata_reg[15]_i_9_n_5 ,\m_axis_tdata_reg[15]_i_9_n_6 ,\m_axis_tdata_reg[15]_i_9_n_7 }),
        .S({\m_axis_tdata[15]_i_10_n_0 ,\m_axis_tdata[15]_i_11_n_0 ,\m_axis_tdata[15]_i_12_n_0 ,\m_axis_tdata[15]_i_13_n_0 }));
  FDRE \m_axis_tdata_reg[1] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[3]_i_1_n_6 ),
        .Q(m_axis_tdata[1]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[2] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[3]_i_1_n_5 ),
        .Q(m_axis_tdata[2]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[3] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[3]_i_1_n_4 ),
        .Q(m_axis_tdata[3]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  CARRY4 \m_axis_tdata_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\m_axis_tdata_reg[3]_i_1_n_0 ,\m_axis_tdata_reg[3]_i_1_n_1 ,\m_axis_tdata_reg[3]_i_1_n_2 ,\m_axis_tdata_reg[3]_i_1_n_3 }),
        .CYINIT(\m_axis_tdata[3]_i_2_n_0 ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\m_axis_tdata_reg[3]_i_1_n_4 ,\m_axis_tdata_reg[3]_i_1_n_5 ,\m_axis_tdata_reg[3]_i_1_n_6 ,\m_axis_tdata_reg[3]_i_1_n_7 }),
        .S({\m_axis_tdata[3]_i_3_n_0 ,\m_axis_tdata[3]_i_4_n_0 ,\m_axis_tdata[3]_i_5_n_0 ,\m_axis_tdata[3]_i_6_n_0 }));
  CARRY4 \m_axis_tdata_reg[3]_i_7 
       (.CI(\m_axis_tdata_reg[15]_i_9_n_0 ),
        .CO({\NLW_m_axis_tdata_reg[3]_i_7_CO_UNCONNECTED [3:1],p_0_in}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_m_axis_tdata_reg[3]_i_7_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  CARRY4 \m_axis_tdata_reg[3]_i_8 
       (.CI(1'b0),
        .CO({\m_axis_tdata_reg[3]_i_8_n_0 ,\m_axis_tdata_reg[3]_i_8_n_1 ,\m_axis_tdata_reg[3]_i_8_n_2 ,\m_axis_tdata_reg[3]_i_8_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\m_axis_tdata_reg[3]_i_9_n_7 }),
        .O(temp_m_axis_tdata[3:0]),
        .S({\m_axis_tdata_reg[3]_i_9_n_4 ,\m_axis_tdata_reg[3]_i_9_n_5 ,\m_axis_tdata_reg[3]_i_9_n_6 ,\m_axis_tdata[3]_i_10_n_0 }));
  CARRY4 \m_axis_tdata_reg[3]_i_9 
       (.CI(1'b0),
        .CO({\m_axis_tdata_reg[3]_i_9_n_0 ,\m_axis_tdata_reg[3]_i_9_n_1 ,\m_axis_tdata_reg[3]_i_9_n_2 ,\m_axis_tdata_reg[3]_i_9_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[11:8]),
        .O({\m_axis_tdata_reg[3]_i_9_n_4 ,\m_axis_tdata_reg[3]_i_9_n_5 ,\m_axis_tdata_reg[3]_i_9_n_6 ,\m_axis_tdata_reg[3]_i_9_n_7 }),
        .S({\m_axis_tdata[3]_i_11_n_0 ,\m_axis_tdata[3]_i_12_n_0 ,\m_axis_tdata[3]_i_13_n_0 ,\m_axis_tdata[3]_i_14_n_0 }));
  FDRE \m_axis_tdata_reg[4] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[7]_i_1_n_7 ),
        .Q(m_axis_tdata[4]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[5] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[7]_i_1_n_6 ),
        .Q(m_axis_tdata[5]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[6] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[7]_i_1_n_5 ),
        .Q(m_axis_tdata[6]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[7] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[7]_i_1_n_4 ),
        .Q(m_axis_tdata[7]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  CARRY4 \m_axis_tdata_reg[7]_i_1 
       (.CI(\m_axis_tdata_reg[3]_i_1_n_0 ),
        .CO({\m_axis_tdata_reg[7]_i_1_n_0 ,\m_axis_tdata_reg[7]_i_1_n_1 ,\m_axis_tdata_reg[7]_i_1_n_2 ,\m_axis_tdata_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\m_axis_tdata_reg[7]_i_1_n_4 ,\m_axis_tdata_reg[7]_i_1_n_5 ,\m_axis_tdata_reg[7]_i_1_n_6 ,\m_axis_tdata_reg[7]_i_1_n_7 }),
        .S({\m_axis_tdata[7]_i_2_n_0 ,\m_axis_tdata[7]_i_3_n_0 ,\m_axis_tdata[7]_i_4_n_0 ,\m_axis_tdata[7]_i_5_n_0 }));
  CARRY4 \m_axis_tdata_reg[7]_i_6 
       (.CI(\m_axis_tdata_reg[3]_i_8_n_0 ),
        .CO({\m_axis_tdata_reg[7]_i_6_n_0 ,\m_axis_tdata_reg[7]_i_6_n_1 ,\m_axis_tdata_reg[7]_i_6_n_2 ,\m_axis_tdata_reg[7]_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(temp_m_axis_tdata[7:4]),
        .S({\m_axis_tdata_reg[7]_i_7_n_4 ,\m_axis_tdata_reg[7]_i_7_n_5 ,\m_axis_tdata_reg[7]_i_7_n_6 ,\m_axis_tdata_reg[7]_i_7_n_7 }));
  CARRY4 \m_axis_tdata_reg[7]_i_7 
       (.CI(\m_axis_tdata_reg[3]_i_9_n_0 ),
        .CO({\m_axis_tdata_reg[7]_i_7_n_0 ,\m_axis_tdata_reg[7]_i_7_n_1 ,\m_axis_tdata_reg[7]_i_7_n_2 ,\m_axis_tdata_reg[7]_i_7_n_3 }),
        .CYINIT(1'b0),
        .DI(s_axis_tdata[15:12]),
        .O({\m_axis_tdata_reg[7]_i_7_n_4 ,\m_axis_tdata_reg[7]_i_7_n_5 ,\m_axis_tdata_reg[7]_i_7_n_6 ,\m_axis_tdata_reg[7]_i_7_n_7 }),
        .S({\m_axis_tdata[7]_i_8_n_0 ,\m_axis_tdata[7]_i_9_n_0 ,\m_axis_tdata[7]_i_10_n_0 ,\m_axis_tdata[7]_i_11_n_0 }));
  FDRE \m_axis_tdata_reg[8] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[11]_i_1_n_7 ),
        .Q(m_axis_tdata[8]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  FDRE \m_axis_tdata_reg[9] 
       (.C(s_axis_aclk),
        .CE(\m_axis_tdata[15]_i_2_n_0 ),
        .D(\m_axis_tdata_reg[11]_i_1_n_6 ),
        .Q(m_axis_tdata[9]),
        .R(\m_axis_tdata[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0C88)) 
    m_axis_tvalid_i_1
       (.I0(s_axis_tlast),
        .I1(s_axis_aresetn),
        .I2(m_axis_tready),
        .I3(m_axis_tvalid_reg_0),
        .O(m_axis_tvalid_i_1_n_0));
  FDRE m_axis_tvalid_reg
       (.C(s_axis_aclk),
        .CE(1'b1),
        .D(m_axis_tvalid_i_1_n_0),
        .Q(m_axis_tvalid_reg_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT1 #(
    .INIT(2'h1)) 
    s_axis_tready_INST_0
       (.I0(m_axis_tvalid_reg_0),
        .O(s_axis_tready));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
