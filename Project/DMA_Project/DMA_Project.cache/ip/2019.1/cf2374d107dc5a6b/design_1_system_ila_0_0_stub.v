// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Sep 23 19:26:25 2023
// Host        : DESKTOP-IDF6QBU running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_system_ila_0_0_stub.v
// Design      : design_1_system_ila_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "bd_f60c,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, SLOT_0_AXIS_TLM_tdata, 
  SLOT_0_AXIS_TLM_tkeep, SLOT_0_AXIS_TLM_tlast, SLOT_0_AXIS_TLM_tready, 
  SLOT_0_AXIS_TLM_tvalid, SLOT_1_AXIS_TLM_tdata, SLOT_1_AXIS_TLM_tkeep, 
  SLOT_1_AXIS_TLM_tlast, SLOT_1_AXIS_TLM_tvalid, SLOT_1_AXIS_TLM_tready)
/* synthesis syn_black_box black_box_pad_pin="clk,SLOT_0_AXIS_TLM_tdata[15:0],SLOT_0_AXIS_TLM_tkeep[1:0],SLOT_0_AXIS_TLM_tlast,SLOT_0_AXIS_TLM_tready,SLOT_0_AXIS_TLM_tvalid,SLOT_1_AXIS_TLM_tdata[15:0],SLOT_1_AXIS_TLM_tkeep[1:0],SLOT_1_AXIS_TLM_tlast,SLOT_1_AXIS_TLM_tvalid,SLOT_1_AXIS_TLM_tready" */;
  input clk;
  input [15:0]SLOT_0_AXIS_TLM_tdata;
  input [1:0]SLOT_0_AXIS_TLM_tkeep;
  input SLOT_0_AXIS_TLM_tlast;
  input SLOT_0_AXIS_TLM_tready;
  input SLOT_0_AXIS_TLM_tvalid;
  input [15:0]SLOT_1_AXIS_TLM_tdata;
  input [1:0]SLOT_1_AXIS_TLM_tkeep;
  input SLOT_1_AXIS_TLM_tlast;
  input SLOT_1_AXIS_TLM_tvalid;
  input SLOT_1_AXIS_TLM_tready;
endmodule
