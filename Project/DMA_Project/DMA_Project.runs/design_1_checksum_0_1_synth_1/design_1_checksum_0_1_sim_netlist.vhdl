-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Sep 23 19:24:27 2023
-- Host        : DESKTOP-IDF6QBU running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_checksum_0_1_sim_netlist.vhdl
-- Design      : design_1_checksum_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_checksum is
  port (
    m_axis_tvalid_reg_0 : out STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_tready : out STD_LOGIC;
    s_axis_tlast : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    s_axis_aclk : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_checksum;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_checksum is
  signal \^m_axis_tdata\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \m_axis_tdata[11]_i_10_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[11]_i_11_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[11]_i_4_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[11]_i_5_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[11]_i_8_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[11]_i_9_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_10_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_11_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_12_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_13_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_1_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_4_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_5_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_6_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[15]_i_7_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_10_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_11_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_12_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_13_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_14_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_4_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_5_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[3]_i_6_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_10_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_11_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_4_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_5_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_8_n_0\ : STD_LOGIC;
  signal \m_axis_tdata[7]_i_9_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_6_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_6_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_6_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_6_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[11]_i_7_n_7\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_3_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_3_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_3_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_3_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_3_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_3_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_3_n_7\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_8_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_8_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_8_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[15]_i_9_n_7\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_8_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_8_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_8_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_8_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[3]_i_9_n_7\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_6_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_6_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_6_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_6_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_0\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_1\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_2\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_3\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_4\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_5\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_6\ : STD_LOGIC;
  signal \m_axis_tdata_reg[7]_i_7_n_7\ : STD_LOGIC;
  signal m_axis_tvalid_i_1_n_0 : STD_LOGIC;
  signal \^m_axis_tvalid_reg_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal temp_m_axis_tdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \NLW_m_axis_tdata_reg[15]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_m_axis_tdata_reg[15]_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_m_axis_tdata_reg[3]_i_7_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_m_axis_tdata_reg[3]_i_7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of m_axis_tvalid_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of s_axis_tready_INST_0 : label is "soft_lutpair0";
begin
  m_axis_tdata(15 downto 0) <= \^m_axis_tdata\(15 downto 0);
  m_axis_tvalid_reg_0 <= \^m_axis_tvalid_reg_0\;
\m_axis_tdata[11]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(1),
      I1 => \^m_axis_tdata\(9),
      O => \m_axis_tdata[11]_i_10_n_0\
    );
\m_axis_tdata[11]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(0),
      I1 => \^m_axis_tdata\(8),
      O => \m_axis_tdata[11]_i_11_n_0\
    );
\m_axis_tdata[11]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(11),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[11]_i_7_n_4\,
      O => \m_axis_tdata[11]_i_2_n_0\
    );
\m_axis_tdata[11]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(10),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[11]_i_7_n_5\,
      O => \m_axis_tdata[11]_i_3_n_0\
    );
\m_axis_tdata[11]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(9),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[11]_i_7_n_6\,
      O => \m_axis_tdata[11]_i_4_n_0\
    );
\m_axis_tdata[11]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(8),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[11]_i_7_n_7\,
      O => \m_axis_tdata[11]_i_5_n_0\
    );
\m_axis_tdata[11]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(3),
      I1 => \^m_axis_tdata\(11),
      O => \m_axis_tdata[11]_i_8_n_0\
    );
\m_axis_tdata[11]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(2),
      I1 => \^m_axis_tdata\(10),
      O => \m_axis_tdata[11]_i_9_n_0\
    );
\m_axis_tdata[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => \^m_axis_tvalid_reg_0\,
      I1 => m_axis_tready,
      I2 => s_axis_aresetn,
      O => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata[15]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(7),
      I1 => \^m_axis_tdata\(15),
      O => \m_axis_tdata[15]_i_10_n_0\
    );
\m_axis_tdata[15]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(6),
      I1 => \^m_axis_tdata\(14),
      O => \m_axis_tdata[15]_i_11_n_0\
    );
\m_axis_tdata[15]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(5),
      I1 => \^m_axis_tdata\(13),
      O => \m_axis_tdata[15]_i_12_n_0\
    );
\m_axis_tdata[15]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(4),
      I1 => \^m_axis_tdata\(12),
      O => \m_axis_tdata[15]_i_13_n_0\
    );
\m_axis_tdata[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s_axis_tvalid,
      I1 => \^m_axis_tvalid_reg_0\,
      O => \m_axis_tdata[15]_i_2_n_0\
    );
\m_axis_tdata[15]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(15),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[15]_i_9_n_4\,
      O => \m_axis_tdata[15]_i_4_n_0\
    );
\m_axis_tdata[15]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(14),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[15]_i_9_n_5\,
      O => \m_axis_tdata[15]_i_5_n_0\
    );
\m_axis_tdata[15]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(13),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[15]_i_9_n_6\,
      O => \m_axis_tdata[15]_i_6_n_0\
    );
\m_axis_tdata[15]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(12),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[15]_i_9_n_7\,
      O => \m_axis_tdata[15]_i_7_n_0\
    );
\m_axis_tdata[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \m_axis_tdata_reg[3]_i_9_n_7\,
      I1 => p_0_in,
      O => \m_axis_tdata[3]_i_10_n_0\
    );
\m_axis_tdata[3]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(11),
      I1 => \^m_axis_tdata\(3),
      O => \m_axis_tdata[3]_i_11_n_0\
    );
\m_axis_tdata[3]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(10),
      I1 => \^m_axis_tdata\(2),
      O => \m_axis_tdata[3]_i_12_n_0\
    );
\m_axis_tdata[3]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(9),
      I1 => \^m_axis_tdata\(1),
      O => \m_axis_tdata[3]_i_13_n_0\
    );
\m_axis_tdata[3]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(8),
      I1 => \^m_axis_tdata\(0),
      O => \m_axis_tdata[3]_i_14_n_0\
    );
\m_axis_tdata[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_0_in,
      I1 => s_axis_tlast,
      O => \m_axis_tdata[3]_i_2_n_0\
    );
\m_axis_tdata[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(3),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[3]_i_9_n_4\,
      O => \m_axis_tdata[3]_i_3_n_0\
    );
\m_axis_tdata[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(2),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[3]_i_9_n_5\,
      O => \m_axis_tdata[3]_i_4_n_0\
    );
\m_axis_tdata[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(1),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[3]_i_9_n_6\,
      O => \m_axis_tdata[3]_i_5_n_0\
    );
\m_axis_tdata[3]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(0),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[3]_i_9_n_7\,
      O => \m_axis_tdata[3]_i_6_n_0\
    );
\m_axis_tdata[7]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(13),
      I1 => \^m_axis_tdata\(5),
      O => \m_axis_tdata[7]_i_10_n_0\
    );
\m_axis_tdata[7]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(12),
      I1 => \^m_axis_tdata\(4),
      O => \m_axis_tdata[7]_i_11_n_0\
    );
\m_axis_tdata[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(7),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[7]_i_7_n_4\,
      O => \m_axis_tdata[7]_i_2_n_0\
    );
\m_axis_tdata[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(6),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[7]_i_7_n_5\,
      O => \m_axis_tdata[7]_i_3_n_0\
    );
\m_axis_tdata[7]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(5),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[7]_i_7_n_6\,
      O => \m_axis_tdata[7]_i_4_n_0\
    );
\m_axis_tdata[7]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"74"
    )
        port map (
      I0 => temp_m_axis_tdata(4),
      I1 => s_axis_tlast,
      I2 => \m_axis_tdata_reg[7]_i_7_n_7\,
      O => \m_axis_tdata[7]_i_5_n_0\
    );
\m_axis_tdata[7]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(15),
      I1 => \^m_axis_tdata\(7),
      O => \m_axis_tdata[7]_i_8_n_0\
    );
\m_axis_tdata[7]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => s_axis_tdata(14),
      I1 => \^m_axis_tdata\(6),
      O => \m_axis_tdata[7]_i_9_n_0\
    );
\m_axis_tdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[3]_i_1_n_7\,
      Q => \^m_axis_tdata\(0),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[11]_i_1_n_5\,
      Q => \^m_axis_tdata\(10),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[11]_i_1_n_4\,
      Q => \^m_axis_tdata\(11),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[7]_i_1_n_0\,
      CO(3) => \m_axis_tdata_reg[11]_i_1_n_0\,
      CO(2) => \m_axis_tdata_reg[11]_i_1_n_1\,
      CO(1) => \m_axis_tdata_reg[11]_i_1_n_2\,
      CO(0) => \m_axis_tdata_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \m_axis_tdata_reg[11]_i_1_n_4\,
      O(2) => \m_axis_tdata_reg[11]_i_1_n_5\,
      O(1) => \m_axis_tdata_reg[11]_i_1_n_6\,
      O(0) => \m_axis_tdata_reg[11]_i_1_n_7\,
      S(3) => \m_axis_tdata[11]_i_2_n_0\,
      S(2) => \m_axis_tdata[11]_i_3_n_0\,
      S(1) => \m_axis_tdata[11]_i_4_n_0\,
      S(0) => \m_axis_tdata[11]_i_5_n_0\
    );
\m_axis_tdata_reg[11]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[7]_i_6_n_0\,
      CO(3) => \m_axis_tdata_reg[11]_i_6_n_0\,
      CO(2) => \m_axis_tdata_reg[11]_i_6_n_1\,
      CO(1) => \m_axis_tdata_reg[11]_i_6_n_2\,
      CO(0) => \m_axis_tdata_reg[11]_i_6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => temp_m_axis_tdata(11 downto 8),
      S(3) => \m_axis_tdata_reg[11]_i_7_n_4\,
      S(2) => \m_axis_tdata_reg[11]_i_7_n_5\,
      S(1) => \m_axis_tdata_reg[11]_i_7_n_6\,
      S(0) => \m_axis_tdata_reg[11]_i_7_n_7\
    );
\m_axis_tdata_reg[11]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[7]_i_7_n_0\,
      CO(3) => \m_axis_tdata_reg[11]_i_7_n_0\,
      CO(2) => \m_axis_tdata_reg[11]_i_7_n_1\,
      CO(1) => \m_axis_tdata_reg[11]_i_7_n_2\,
      CO(0) => \m_axis_tdata_reg[11]_i_7_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(3 downto 0),
      O(3) => \m_axis_tdata_reg[11]_i_7_n_4\,
      O(2) => \m_axis_tdata_reg[11]_i_7_n_5\,
      O(1) => \m_axis_tdata_reg[11]_i_7_n_6\,
      O(0) => \m_axis_tdata_reg[11]_i_7_n_7\,
      S(3) => \m_axis_tdata[11]_i_8_n_0\,
      S(2) => \m_axis_tdata[11]_i_9_n_0\,
      S(1) => \m_axis_tdata[11]_i_10_n_0\,
      S(0) => \m_axis_tdata[11]_i_11_n_0\
    );
\m_axis_tdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[15]_i_3_n_7\,
      Q => \^m_axis_tdata\(12),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[15]_i_3_n_6\,
      Q => \^m_axis_tdata\(13),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[15]_i_3_n_5\,
      Q => \^m_axis_tdata\(14),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[15]_i_3_n_4\,
      Q => \^m_axis_tdata\(15),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[15]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[11]_i_1_n_0\,
      CO(3) => \NLW_m_axis_tdata_reg[15]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \m_axis_tdata_reg[15]_i_3_n_1\,
      CO(1) => \m_axis_tdata_reg[15]_i_3_n_2\,
      CO(0) => \m_axis_tdata_reg[15]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \m_axis_tdata_reg[15]_i_3_n_4\,
      O(2) => \m_axis_tdata_reg[15]_i_3_n_5\,
      O(1) => \m_axis_tdata_reg[15]_i_3_n_6\,
      O(0) => \m_axis_tdata_reg[15]_i_3_n_7\,
      S(3) => \m_axis_tdata[15]_i_4_n_0\,
      S(2) => \m_axis_tdata[15]_i_5_n_0\,
      S(1) => \m_axis_tdata[15]_i_6_n_0\,
      S(0) => \m_axis_tdata[15]_i_7_n_0\
    );
\m_axis_tdata_reg[15]_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[11]_i_6_n_0\,
      CO(3) => \NLW_m_axis_tdata_reg[15]_i_8_CO_UNCONNECTED\(3),
      CO(2) => \m_axis_tdata_reg[15]_i_8_n_1\,
      CO(1) => \m_axis_tdata_reg[15]_i_8_n_2\,
      CO(0) => \m_axis_tdata_reg[15]_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => temp_m_axis_tdata(15 downto 12),
      S(3) => \m_axis_tdata_reg[15]_i_9_n_4\,
      S(2) => \m_axis_tdata_reg[15]_i_9_n_5\,
      S(1) => \m_axis_tdata_reg[15]_i_9_n_6\,
      S(0) => \m_axis_tdata_reg[15]_i_9_n_7\
    );
\m_axis_tdata_reg[15]_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[11]_i_7_n_0\,
      CO(3) => \m_axis_tdata_reg[15]_i_9_n_0\,
      CO(2) => \m_axis_tdata_reg[15]_i_9_n_1\,
      CO(1) => \m_axis_tdata_reg[15]_i_9_n_2\,
      CO(0) => \m_axis_tdata_reg[15]_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(7 downto 4),
      O(3) => \m_axis_tdata_reg[15]_i_9_n_4\,
      O(2) => \m_axis_tdata_reg[15]_i_9_n_5\,
      O(1) => \m_axis_tdata_reg[15]_i_9_n_6\,
      O(0) => \m_axis_tdata_reg[15]_i_9_n_7\,
      S(3) => \m_axis_tdata[15]_i_10_n_0\,
      S(2) => \m_axis_tdata[15]_i_11_n_0\,
      S(1) => \m_axis_tdata[15]_i_12_n_0\,
      S(0) => \m_axis_tdata[15]_i_13_n_0\
    );
\m_axis_tdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[3]_i_1_n_6\,
      Q => \^m_axis_tdata\(1),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[3]_i_1_n_5\,
      Q => \^m_axis_tdata\(2),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[3]_i_1_n_4\,
      Q => \^m_axis_tdata\(3),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \m_axis_tdata_reg[3]_i_1_n_0\,
      CO(2) => \m_axis_tdata_reg[3]_i_1_n_1\,
      CO(1) => \m_axis_tdata_reg[3]_i_1_n_2\,
      CO(0) => \m_axis_tdata_reg[3]_i_1_n_3\,
      CYINIT => \m_axis_tdata[3]_i_2_n_0\,
      DI(3 downto 0) => B"0000",
      O(3) => \m_axis_tdata_reg[3]_i_1_n_4\,
      O(2) => \m_axis_tdata_reg[3]_i_1_n_5\,
      O(1) => \m_axis_tdata_reg[3]_i_1_n_6\,
      O(0) => \m_axis_tdata_reg[3]_i_1_n_7\,
      S(3) => \m_axis_tdata[3]_i_3_n_0\,
      S(2) => \m_axis_tdata[3]_i_4_n_0\,
      S(1) => \m_axis_tdata[3]_i_5_n_0\,
      S(0) => \m_axis_tdata[3]_i_6_n_0\
    );
\m_axis_tdata_reg[3]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[15]_i_9_n_0\,
      CO(3 downto 1) => \NLW_m_axis_tdata_reg[3]_i_7_CO_UNCONNECTED\(3 downto 1),
      CO(0) => p_0_in,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m_axis_tdata_reg[3]_i_7_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\m_axis_tdata_reg[3]_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \m_axis_tdata_reg[3]_i_8_n_0\,
      CO(2) => \m_axis_tdata_reg[3]_i_8_n_1\,
      CO(1) => \m_axis_tdata_reg[3]_i_8_n_2\,
      CO(0) => \m_axis_tdata_reg[3]_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \m_axis_tdata_reg[3]_i_9_n_7\,
      O(3 downto 0) => temp_m_axis_tdata(3 downto 0),
      S(3) => \m_axis_tdata_reg[3]_i_9_n_4\,
      S(2) => \m_axis_tdata_reg[3]_i_9_n_5\,
      S(1) => \m_axis_tdata_reg[3]_i_9_n_6\,
      S(0) => \m_axis_tdata[3]_i_10_n_0\
    );
\m_axis_tdata_reg[3]_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \m_axis_tdata_reg[3]_i_9_n_0\,
      CO(2) => \m_axis_tdata_reg[3]_i_9_n_1\,
      CO(1) => \m_axis_tdata_reg[3]_i_9_n_2\,
      CO(0) => \m_axis_tdata_reg[3]_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(11 downto 8),
      O(3) => \m_axis_tdata_reg[3]_i_9_n_4\,
      O(2) => \m_axis_tdata_reg[3]_i_9_n_5\,
      O(1) => \m_axis_tdata_reg[3]_i_9_n_6\,
      O(0) => \m_axis_tdata_reg[3]_i_9_n_7\,
      S(3) => \m_axis_tdata[3]_i_11_n_0\,
      S(2) => \m_axis_tdata[3]_i_12_n_0\,
      S(1) => \m_axis_tdata[3]_i_13_n_0\,
      S(0) => \m_axis_tdata[3]_i_14_n_0\
    );
\m_axis_tdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[7]_i_1_n_7\,
      Q => \^m_axis_tdata\(4),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[7]_i_1_n_6\,
      Q => \^m_axis_tdata\(5),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[7]_i_1_n_5\,
      Q => \^m_axis_tdata\(6),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[7]_i_1_n_4\,
      Q => \^m_axis_tdata\(7),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[3]_i_1_n_0\,
      CO(3) => \m_axis_tdata_reg[7]_i_1_n_0\,
      CO(2) => \m_axis_tdata_reg[7]_i_1_n_1\,
      CO(1) => \m_axis_tdata_reg[7]_i_1_n_2\,
      CO(0) => \m_axis_tdata_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \m_axis_tdata_reg[7]_i_1_n_4\,
      O(2) => \m_axis_tdata_reg[7]_i_1_n_5\,
      O(1) => \m_axis_tdata_reg[7]_i_1_n_6\,
      O(0) => \m_axis_tdata_reg[7]_i_1_n_7\,
      S(3) => \m_axis_tdata[7]_i_2_n_0\,
      S(2) => \m_axis_tdata[7]_i_3_n_0\,
      S(1) => \m_axis_tdata[7]_i_4_n_0\,
      S(0) => \m_axis_tdata[7]_i_5_n_0\
    );
\m_axis_tdata_reg[7]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[3]_i_8_n_0\,
      CO(3) => \m_axis_tdata_reg[7]_i_6_n_0\,
      CO(2) => \m_axis_tdata_reg[7]_i_6_n_1\,
      CO(1) => \m_axis_tdata_reg[7]_i_6_n_2\,
      CO(0) => \m_axis_tdata_reg[7]_i_6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => temp_m_axis_tdata(7 downto 4),
      S(3) => \m_axis_tdata_reg[7]_i_7_n_4\,
      S(2) => \m_axis_tdata_reg[7]_i_7_n_5\,
      S(1) => \m_axis_tdata_reg[7]_i_7_n_6\,
      S(0) => \m_axis_tdata_reg[7]_i_7_n_7\
    );
\m_axis_tdata_reg[7]_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_axis_tdata_reg[3]_i_9_n_0\,
      CO(3) => \m_axis_tdata_reg[7]_i_7_n_0\,
      CO(2) => \m_axis_tdata_reg[7]_i_7_n_1\,
      CO(1) => \m_axis_tdata_reg[7]_i_7_n_2\,
      CO(0) => \m_axis_tdata_reg[7]_i_7_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => s_axis_tdata(15 downto 12),
      O(3) => \m_axis_tdata_reg[7]_i_7_n_4\,
      O(2) => \m_axis_tdata_reg[7]_i_7_n_5\,
      O(1) => \m_axis_tdata_reg[7]_i_7_n_6\,
      O(0) => \m_axis_tdata_reg[7]_i_7_n_7\,
      S(3) => \m_axis_tdata[7]_i_8_n_0\,
      S(2) => \m_axis_tdata[7]_i_9_n_0\,
      S(1) => \m_axis_tdata[7]_i_10_n_0\,
      S(0) => \m_axis_tdata[7]_i_11_n_0\
    );
\m_axis_tdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[11]_i_1_n_7\,
      Q => \^m_axis_tdata\(8),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
\m_axis_tdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => \m_axis_tdata[15]_i_2_n_0\,
      D => \m_axis_tdata_reg[11]_i_1_n_6\,
      Q => \^m_axis_tdata\(9),
      R => \m_axis_tdata[15]_i_1_n_0\
    );
m_axis_tvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0C88"
    )
        port map (
      I0 => s_axis_tlast,
      I1 => s_axis_aresetn,
      I2 => m_axis_tready,
      I3 => \^m_axis_tvalid_reg_0\,
      O => m_axis_tvalid_i_1_n_0
    );
m_axis_tvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axis_aclk,
      CE => '1',
      D => m_axis_tvalid_i_1_n_0,
      Q => \^m_axis_tvalid_reg_0\,
      R => '0'
    );
s_axis_tready_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^m_axis_tvalid_reg_0\,
      O => s_axis_tready
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    m_axis_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    s_axis_aclk : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_checksum_0_1,checksum,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "checksum,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  signal \^m_axis_tlast\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m_axis TLAST";
  attribute X_INTERFACE_INFO of m_axis_tready : signal is "xilinx.com:interface:axis:1.0 m_axis TREADY";
  attribute X_INTERFACE_INFO of m_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m_axis TVALID";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m_axis_tvalid : signal is "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axis_aclk : signal is "xilinx.com:signal:clock:1.0 s_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER of s_axis_aclk : signal is "XIL_INTERFACENAME s_axis_aclk, ASSOCIATED_BUSIF s_axis, ASSOCIATED_RESET s_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_2_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 s_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of s_axis_aresetn : signal is "XIL_INTERFACENAME s_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s_axis TLAST";
  attribute X_INTERFACE_INFO of s_axis_tready : signal is "xilinx.com:interface:axis:1.0 s_axis TREADY";
  attribute X_INTERFACE_INFO of s_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s_axis TVALID";
  attribute X_INTERFACE_PARAMETER of s_axis_tvalid : signal is "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_2_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m_axis TDATA";
  attribute X_INTERFACE_INFO of m_axis_tkeep : signal is "xilinx.com:interface:axis:1.0 m_axis TKEEP";
  attribute X_INTERFACE_INFO of s_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s_axis TDATA";
  attribute X_INTERFACE_INFO of s_axis_tkeep : signal is "xilinx.com:interface:axis:1.0 s_axis TKEEP";
begin
  m_axis_tkeep(1) <= \<const1>\;
  m_axis_tkeep(0) <= \<const1>\;
  m_axis_tlast <= \^m_axis_tlast\;
  m_axis_tvalid <= \^m_axis_tlast\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_checksum
     port map (
      m_axis_tdata(15 downto 0) => m_axis_tdata(15 downto 0),
      m_axis_tready => m_axis_tready,
      m_axis_tvalid_reg_0 => \^m_axis_tlast\,
      s_axis_aclk => s_axis_aclk,
      s_axis_aresetn => s_axis_aresetn,
      s_axis_tdata(15 downto 0) => s_axis_tdata(15 downto 0),
      s_axis_tlast => s_axis_tlast,
      s_axis_tready => s_axis_tready,
      s_axis_tvalid => s_axis_tvalid
    );
end STRUCTURE;
