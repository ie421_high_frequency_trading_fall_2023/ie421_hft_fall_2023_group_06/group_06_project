-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Sep 23 19:24:27 2023
-- Host        : DESKTOP-IDF6QBU running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               D:/xilinx/Ethernet_Starter_Code_v2_just_need_to_update_checksum_ipv4noUDPok/DMA_Project/DMA_Project.srcs/sources_1/bd/design_1/ip/design_1_checksum_0_1/design_1_checksum_0_1_stub.vhdl
-- Design      : design_1_checksum_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_checksum_0_1 is
  Port ( 
    m_axis_tdata : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tvalid : out STD_LOGIC;
    s_axis_aclk : in STD_LOGIC;
    s_axis_aresetn : in STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC
  );

end design_1_checksum_0_1;

architecture stub of design_1_checksum_0_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "m_axis_tdata[15:0],m_axis_tkeep[1:0],m_axis_tlast,m_axis_tready,m_axis_tvalid,s_axis_aclk,s_axis_aresetn,s_axis_tdata[15:0],s_axis_tkeep[1:0],s_axis_tlast,s_axis_tready,s_axis_tvalid";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "checksum,Vivado 2019.1";
begin
end;
