// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Sep 23 19:24:27 2023
// Host        : DESKTOP-IDF6QBU running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/xilinx/Ethernet_Starter_Code_v2_just_need_to_update_checksum_ipv4noUDPok/DMA_Project/DMA_Project.srcs/sources_1/bd/design_1/ip/design_1_checksum_0_1/design_1_checksum_0_1_stub.v
// Design      : design_1_checksum_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "checksum,Vivado 2019.1" *)
module design_1_checksum_0_1(m_axis_tdata, m_axis_tkeep, m_axis_tlast, 
  m_axis_tready, m_axis_tvalid, s_axis_aclk, s_axis_aresetn, s_axis_tdata, s_axis_tkeep, 
  s_axis_tlast, s_axis_tready, s_axis_tvalid)
/* synthesis syn_black_box black_box_pad_pin="m_axis_tdata[15:0],m_axis_tkeep[1:0],m_axis_tlast,m_axis_tready,m_axis_tvalid,s_axis_aclk,s_axis_aresetn,s_axis_tdata[15:0],s_axis_tkeep[1:0],s_axis_tlast,s_axis_tready,s_axis_tvalid" */;
  output [15:0]m_axis_tdata;
  output [1:0]m_axis_tkeep;
  output m_axis_tlast;
  input m_axis_tready;
  output m_axis_tvalid;
  input s_axis_aclk;
  input s_axis_aresetn;
  input [15:0]s_axis_tdata;
  input [1:0]s_axis_tkeep;
  input s_axis_tlast;
  output s_axis_tready;
  input s_axis_tvalid;
endmodule
