`timescale 1ns / 1ps

module checksum(
    output reg [15:0] m_axis_tdata,
    output wire [1:0] m_axis_tkeep,
    output reg m_axis_tlast,
    input m_axis_tready,
    output reg m_axis_tvalid,
   
    input s_axis_aclk,
    input s_axis_aresetn,
   
    input [15:0] s_axis_tdata,
    input [1:0] s_axis_tkeep,
    input s_axis_tlast,
    output s_axis_tready,
    input s_axis_tvalid
    );
    assign m_axis_tkeep = 2'b11;
    assign s_axis_tready = ~m_axis_tvalid;
    wire[16:0] sum;
    wire [15:0] flipped_data = { s_axis_tdata[7:0],s_axis_tdata[15:8]};
    assign sum = flipped_data + m_axis_tdata;
    wire [15:0]temp_m_axis_tdata;
    assign temp_m_axis_tdata = sum[15:0] + sum[16];
    
    always@(posedge s_axis_aclk) begin
        if(!s_axis_aresetn) begin
            m_axis_tdata <= 0;
            m_axis_tvalid <= 0;
            m_axis_tlast <= 0;
        end
        else begin
            if(s_axis_tlast) begin
                m_axis_tvalid <= 1;
                m_axis_tlast <= 1;
            end
            // after sending data back to DMA, reset everything to 0
            if(m_axis_tready && m_axis_tvalid) begin
                m_axis_tdata <= 0;
                m_axis_tvalid <= 0;
                m_axis_tlast <= 0;
            end
            
            // DMA send data to CHECKSUM    
            if(s_axis_tvalid && s_axis_tready) begin
                if(s_axis_tlast) 
                    m_axis_tdata <= ~temp_m_axis_tdata;

                else 
                    m_axis_tdata <= temp_m_axis_tdata;
                     
            end
        end
    end
    
   endmodule