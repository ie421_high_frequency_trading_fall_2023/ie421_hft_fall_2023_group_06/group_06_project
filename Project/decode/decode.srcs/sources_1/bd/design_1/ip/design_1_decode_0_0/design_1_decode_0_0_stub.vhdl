-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Dec 14 10:40:58 2023
-- Host        : jianjian running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/Users/11755/Downloads/decode_multi/decode/decode.srcs/sources_1/bd/design_1/ip/design_1_decode_0_0/design_1_decode_0_0_stub.vhdl
-- Design      : design_1_decode_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_decode_0_0 is
  Port ( 
    s_axi_CTL_AWADDR : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s_axi_CTL_AWVALID : in STD_LOGIC;
    s_axi_CTL_AWREADY : out STD_LOGIC;
    s_axi_CTL_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CTL_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CTL_WVALID : in STD_LOGIC;
    s_axi_CTL_WREADY : out STD_LOGIC;
    s_axi_CTL_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CTL_BVALID : out STD_LOGIC;
    s_axi_CTL_BREADY : in STD_LOGIC;
    s_axi_CTL_ARADDR : in STD_LOGIC_VECTOR ( 6 downto 0 );
    s_axi_CTL_ARVALID : in STD_LOGIC;
    s_axi_CTL_ARREADY : out STD_LOGIC;
    s_axi_CTL_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CTL_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CTL_RVALID : out STD_LOGIC;
    s_axi_CTL_RREADY : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    interrupt : out STD_LOGIC;
    m_axi_MESSAGE_AWADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_MESSAGE_AWLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_MESSAGE_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_MESSAGE_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_MESSAGE_AWLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_MESSAGE_AWREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_MESSAGE_AWCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_MESSAGE_AWPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_MESSAGE_AWQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_MESSAGE_AWVALID : out STD_LOGIC;
    m_axi_MESSAGE_AWREADY : in STD_LOGIC;
    m_axi_MESSAGE_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_MESSAGE_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_MESSAGE_WLAST : out STD_LOGIC;
    m_axi_MESSAGE_WVALID : out STD_LOGIC;
    m_axi_MESSAGE_WREADY : in STD_LOGIC;
    m_axi_MESSAGE_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_MESSAGE_BVALID : in STD_LOGIC;
    m_axi_MESSAGE_BREADY : out STD_LOGIC;
    m_axi_MESSAGE_ARADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_MESSAGE_ARLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_MESSAGE_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_MESSAGE_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_MESSAGE_ARLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_MESSAGE_ARREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_MESSAGE_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_MESSAGE_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_MESSAGE_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_MESSAGE_ARVALID : out STD_LOGIC;
    m_axi_MESSAGE_ARREADY : in STD_LOGIC;
    m_axi_MESSAGE_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_MESSAGE_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_MESSAGE_RLAST : in STD_LOGIC;
    m_axi_MESSAGE_RVALID : in STD_LOGIC;
    m_axi_MESSAGE_RREADY : out STD_LOGIC;
    m_axi_PARSED_DATA_AWADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_PARSED_DATA_AWLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_PARSED_DATA_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_PARSED_DATA_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_PARSED_DATA_AWLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_PARSED_DATA_AWREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_PARSED_DATA_AWCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_PARSED_DATA_AWPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_PARSED_DATA_AWQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_PARSED_DATA_AWVALID : out STD_LOGIC;
    m_axi_PARSED_DATA_AWREADY : in STD_LOGIC;
    m_axi_PARSED_DATA_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_PARSED_DATA_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_PARSED_DATA_WLAST : out STD_LOGIC;
    m_axi_PARSED_DATA_WVALID : out STD_LOGIC;
    m_axi_PARSED_DATA_WREADY : in STD_LOGIC;
    m_axi_PARSED_DATA_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_PARSED_DATA_BVALID : in STD_LOGIC;
    m_axi_PARSED_DATA_BREADY : out STD_LOGIC;
    m_axi_PARSED_DATA_ARADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_PARSED_DATA_ARLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_PARSED_DATA_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_PARSED_DATA_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_PARSED_DATA_ARLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_PARSED_DATA_ARREGION : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_PARSED_DATA_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_PARSED_DATA_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_PARSED_DATA_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_PARSED_DATA_ARVALID : out STD_LOGIC;
    m_axi_PARSED_DATA_ARREADY : in STD_LOGIC;
    m_axi_PARSED_DATA_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_PARSED_DATA_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_PARSED_DATA_RLAST : in STD_LOGIC;
    m_axi_PARSED_DATA_RVALID : in STD_LOGIC;
    m_axi_PARSED_DATA_RREADY : out STD_LOGIC
  );

end design_1_decode_0_0;

architecture stub of design_1_decode_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "s_axi_CTL_AWADDR[6:0],s_axi_CTL_AWVALID,s_axi_CTL_AWREADY,s_axi_CTL_WDATA[31:0],s_axi_CTL_WSTRB[3:0],s_axi_CTL_WVALID,s_axi_CTL_WREADY,s_axi_CTL_BRESP[1:0],s_axi_CTL_BVALID,s_axi_CTL_BREADY,s_axi_CTL_ARADDR[6:0],s_axi_CTL_ARVALID,s_axi_CTL_ARREADY,s_axi_CTL_RDATA[31:0],s_axi_CTL_RRESP[1:0],s_axi_CTL_RVALID,s_axi_CTL_RREADY,ap_clk,ap_rst_n,interrupt,m_axi_MESSAGE_AWADDR[31:0],m_axi_MESSAGE_AWLEN[7:0],m_axi_MESSAGE_AWSIZE[2:0],m_axi_MESSAGE_AWBURST[1:0],m_axi_MESSAGE_AWLOCK[1:0],m_axi_MESSAGE_AWREGION[3:0],m_axi_MESSAGE_AWCACHE[3:0],m_axi_MESSAGE_AWPROT[2:0],m_axi_MESSAGE_AWQOS[3:0],m_axi_MESSAGE_AWVALID,m_axi_MESSAGE_AWREADY,m_axi_MESSAGE_WDATA[31:0],m_axi_MESSAGE_WSTRB[3:0],m_axi_MESSAGE_WLAST,m_axi_MESSAGE_WVALID,m_axi_MESSAGE_WREADY,m_axi_MESSAGE_BRESP[1:0],m_axi_MESSAGE_BVALID,m_axi_MESSAGE_BREADY,m_axi_MESSAGE_ARADDR[31:0],m_axi_MESSAGE_ARLEN[7:0],m_axi_MESSAGE_ARSIZE[2:0],m_axi_MESSAGE_ARBURST[1:0],m_axi_MESSAGE_ARLOCK[1:0],m_axi_MESSAGE_ARREGION[3:0],m_axi_MESSAGE_ARCACHE[3:0],m_axi_MESSAGE_ARPROT[2:0],m_axi_MESSAGE_ARQOS[3:0],m_axi_MESSAGE_ARVALID,m_axi_MESSAGE_ARREADY,m_axi_MESSAGE_RDATA[31:0],m_axi_MESSAGE_RRESP[1:0],m_axi_MESSAGE_RLAST,m_axi_MESSAGE_RVALID,m_axi_MESSAGE_RREADY,m_axi_PARSED_DATA_AWADDR[31:0],m_axi_PARSED_DATA_AWLEN[7:0],m_axi_PARSED_DATA_AWSIZE[2:0],m_axi_PARSED_DATA_AWBURST[1:0],m_axi_PARSED_DATA_AWLOCK[1:0],m_axi_PARSED_DATA_AWREGION[3:0],m_axi_PARSED_DATA_AWCACHE[3:0],m_axi_PARSED_DATA_AWPROT[2:0],m_axi_PARSED_DATA_AWQOS[3:0],m_axi_PARSED_DATA_AWVALID,m_axi_PARSED_DATA_AWREADY,m_axi_PARSED_DATA_WDATA[31:0],m_axi_PARSED_DATA_WSTRB[3:0],m_axi_PARSED_DATA_WLAST,m_axi_PARSED_DATA_WVALID,m_axi_PARSED_DATA_WREADY,m_axi_PARSED_DATA_BRESP[1:0],m_axi_PARSED_DATA_BVALID,m_axi_PARSED_DATA_BREADY,m_axi_PARSED_DATA_ARADDR[31:0],m_axi_PARSED_DATA_ARLEN[7:0],m_axi_PARSED_DATA_ARSIZE[2:0],m_axi_PARSED_DATA_ARBURST[1:0],m_axi_PARSED_DATA_ARLOCK[1:0],m_axi_PARSED_DATA_ARREGION[3:0],m_axi_PARSED_DATA_ARCACHE[3:0],m_axi_PARSED_DATA_ARPROT[2:0],m_axi_PARSED_DATA_ARQOS[3:0],m_axi_PARSED_DATA_ARVALID,m_axi_PARSED_DATA_ARREADY,m_axi_PARSED_DATA_RDATA[31:0],m_axi_PARSED_DATA_RRESP[1:0],m_axi_PARSED_DATA_RLAST,m_axi_PARSED_DATA_RVALID,m_axi_PARSED_DATA_RREADY";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "decode,Vivado 2019.1";
begin
end;
