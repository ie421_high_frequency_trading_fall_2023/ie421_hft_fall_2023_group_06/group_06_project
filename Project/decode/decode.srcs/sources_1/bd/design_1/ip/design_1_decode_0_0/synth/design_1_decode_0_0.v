// (c) Copyright 1995-2023 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:hls:decode:1.0
// IP Revision: 2113337614

(* X_CORE_INFO = "decode,Vivado 2019.1" *)
(* CHECK_LICENSE_TYPE = "design_1_decode_0_0,decode,{}" *)
(* CORE_GENERATION_INFO = "design_1_decode_0_0,decode,{x_ipProduct=Vivado 2019.1,x_ipVendor=xilinx.com,x_ipLibrary=hls,x_ipName=decode,x_ipVersion=1.0,x_ipCoreRevision=2113337614,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED,C_S_AXI_CTL_ADDR_WIDTH=7,C_S_AXI_CTL_DATA_WIDTH=32,C_M_AXI_MESSAGE_ID_WIDTH=1,C_M_AXI_MESSAGE_ADDR_WIDTH=32,C_M_AXI_MESSAGE_DATA_WIDTH=32,C_M_AXI_MESSAGE_AWUSER_WIDTH=1,C_M_AXI_MESSAGE_ARUSER_WIDTH=1,C_M_AXI_MESSAGE_WUSER_WIDTH=1,C_M_AXI_MESSAGE_RUSER_WIDTH=1,C_M_AXI_MESSAGE_BUSER_WIDTH=1,C_M_AXI_MESSAGE\
_USER_VALUE=0x00000000,C_M_AXI_MESSAGE_PROT_VALUE=000,C_M_AXI_MESSAGE_CACHE_VALUE=0011,C_M_AXI_PARSED_DATA_ID_WIDTH=1,C_M_AXI_PARSED_DATA_ADDR_WIDTH=32,C_M_AXI_PARSED_DATA_DATA_WIDTH=32,C_M_AXI_PARSED_DATA_AWUSER_WIDTH=1,C_M_AXI_PARSED_DATA_ARUSER_WIDTH=1,C_M_AXI_PARSED_DATA_WUSER_WIDTH=1,C_M_AXI_PARSED_DATA_RUSER_WIDTH=1,C_M_AXI_PARSED_DATA_BUSER_WIDTH=1,C_M_AXI_PARSED_DATA_USER_VALUE=0x00000000,C_M_AXI_PARSED_DATA_PROT_VALUE=000,C_M_AXI_PARSED_DATA_CACHE_VALUE=0011}" *)
(* IP_DEFINITION_SOURCE = "HLS" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_decode_0_0 (
  s_axi_CTL_AWADDR,
  s_axi_CTL_AWVALID,
  s_axi_CTL_AWREADY,
  s_axi_CTL_WDATA,
  s_axi_CTL_WSTRB,
  s_axi_CTL_WVALID,
  s_axi_CTL_WREADY,
  s_axi_CTL_BRESP,
  s_axi_CTL_BVALID,
  s_axi_CTL_BREADY,
  s_axi_CTL_ARADDR,
  s_axi_CTL_ARVALID,
  s_axi_CTL_ARREADY,
  s_axi_CTL_RDATA,
  s_axi_CTL_RRESP,
  s_axi_CTL_RVALID,
  s_axi_CTL_RREADY,
  ap_clk,
  ap_rst_n,
  interrupt,
  m_axi_MESSAGE_AWADDR,
  m_axi_MESSAGE_AWLEN,
  m_axi_MESSAGE_AWSIZE,
  m_axi_MESSAGE_AWBURST,
  m_axi_MESSAGE_AWLOCK,
  m_axi_MESSAGE_AWREGION,
  m_axi_MESSAGE_AWCACHE,
  m_axi_MESSAGE_AWPROT,
  m_axi_MESSAGE_AWQOS,
  m_axi_MESSAGE_AWVALID,
  m_axi_MESSAGE_AWREADY,
  m_axi_MESSAGE_WDATA,
  m_axi_MESSAGE_WSTRB,
  m_axi_MESSAGE_WLAST,
  m_axi_MESSAGE_WVALID,
  m_axi_MESSAGE_WREADY,
  m_axi_MESSAGE_BRESP,
  m_axi_MESSAGE_BVALID,
  m_axi_MESSAGE_BREADY,
  m_axi_MESSAGE_ARADDR,
  m_axi_MESSAGE_ARLEN,
  m_axi_MESSAGE_ARSIZE,
  m_axi_MESSAGE_ARBURST,
  m_axi_MESSAGE_ARLOCK,
  m_axi_MESSAGE_ARREGION,
  m_axi_MESSAGE_ARCACHE,
  m_axi_MESSAGE_ARPROT,
  m_axi_MESSAGE_ARQOS,
  m_axi_MESSAGE_ARVALID,
  m_axi_MESSAGE_ARREADY,
  m_axi_MESSAGE_RDATA,
  m_axi_MESSAGE_RRESP,
  m_axi_MESSAGE_RLAST,
  m_axi_MESSAGE_RVALID,
  m_axi_MESSAGE_RREADY,
  m_axi_PARSED_DATA_AWADDR,
  m_axi_PARSED_DATA_AWLEN,
  m_axi_PARSED_DATA_AWSIZE,
  m_axi_PARSED_DATA_AWBURST,
  m_axi_PARSED_DATA_AWLOCK,
  m_axi_PARSED_DATA_AWREGION,
  m_axi_PARSED_DATA_AWCACHE,
  m_axi_PARSED_DATA_AWPROT,
  m_axi_PARSED_DATA_AWQOS,
  m_axi_PARSED_DATA_AWVALID,
  m_axi_PARSED_DATA_AWREADY,
  m_axi_PARSED_DATA_WDATA,
  m_axi_PARSED_DATA_WSTRB,
  m_axi_PARSED_DATA_WLAST,
  m_axi_PARSED_DATA_WVALID,
  m_axi_PARSED_DATA_WREADY,
  m_axi_PARSED_DATA_BRESP,
  m_axi_PARSED_DATA_BVALID,
  m_axi_PARSED_DATA_BREADY,
  m_axi_PARSED_DATA_ARADDR,
  m_axi_PARSED_DATA_ARLEN,
  m_axi_PARSED_DATA_ARSIZE,
  m_axi_PARSED_DATA_ARBURST,
  m_axi_PARSED_DATA_ARLOCK,
  m_axi_PARSED_DATA_ARREGION,
  m_axi_PARSED_DATA_ARCACHE,
  m_axi_PARSED_DATA_ARPROT,
  m_axi_PARSED_DATA_ARQOS,
  m_axi_PARSED_DATA_ARVALID,
  m_axi_PARSED_DATA_ARREADY,
  m_axi_PARSED_DATA_RDATA,
  m_axi_PARSED_DATA_RRESP,
  m_axi_PARSED_DATA_RLAST,
  m_axi_PARSED_DATA_RVALID,
  m_axi_PARSED_DATA_RREADY
);

(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL AWADDR" *)
input wire [6 : 0] s_axi_CTL_AWADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL AWVALID" *)
input wire s_axi_CTL_AWVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL AWREADY" *)
output wire s_axi_CTL_AWREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL WDATA" *)
input wire [31 : 0] s_axi_CTL_WDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL WSTRB" *)
input wire [3 : 0] s_axi_CTL_WSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL WVALID" *)
input wire s_axi_CTL_WVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL WREADY" *)
output wire s_axi_CTL_WREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL BRESP" *)
output wire [1 : 0] s_axi_CTL_BRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL BVALID" *)
output wire s_axi_CTL_BVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL BREADY" *)
input wire s_axi_CTL_BREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL ARADDR" *)
input wire [6 : 0] s_axi_CTL_ARADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL ARVALID" *)
input wire s_axi_CTL_ARVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL ARREADY" *)
output wire s_axi_CTL_ARREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL RDATA" *)
output wire [31 : 0] s_axi_CTL_RDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL RRESP" *)
output wire [1 : 0] s_axi_CTL_RRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL RVALID" *)
output wire s_axi_CTL_RVALID;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_CTL, ADDR_WIDTH 7, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THR\
EADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_CTL RREADY" *)
input wire s_axi_CTL_RREADY;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CTL:m_axi_MESSAGE:m_axi_PARSED_DATA, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 ap_clk CLK" *)
input wire ap_clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *)
input wire ap_rst_n;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, PortWidth 1" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *)
output wire interrupt;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWADDR" *)
output wire [31 : 0] m_axi_MESSAGE_AWADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWLEN" *)
output wire [7 : 0] m_axi_MESSAGE_AWLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWSIZE" *)
output wire [2 : 0] m_axi_MESSAGE_AWSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWBURST" *)
output wire [1 : 0] m_axi_MESSAGE_AWBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWLOCK" *)
output wire [1 : 0] m_axi_MESSAGE_AWLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWREGION" *)
output wire [3 : 0] m_axi_MESSAGE_AWREGION;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWCACHE" *)
output wire [3 : 0] m_axi_MESSAGE_AWCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWPROT" *)
output wire [2 : 0] m_axi_MESSAGE_AWPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWQOS" *)
output wire [3 : 0] m_axi_MESSAGE_AWQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWVALID" *)
output wire m_axi_MESSAGE_AWVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE AWREADY" *)
input wire m_axi_MESSAGE_AWREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE WDATA" *)
output wire [31 : 0] m_axi_MESSAGE_WDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE WSTRB" *)
output wire [3 : 0] m_axi_MESSAGE_WSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE WLAST" *)
output wire m_axi_MESSAGE_WLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE WVALID" *)
output wire m_axi_MESSAGE_WVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE WREADY" *)
input wire m_axi_MESSAGE_WREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE BRESP" *)
input wire [1 : 0] m_axi_MESSAGE_BRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE BVALID" *)
input wire m_axi_MESSAGE_BVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE BREADY" *)
output wire m_axi_MESSAGE_BREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARADDR" *)
output wire [31 : 0] m_axi_MESSAGE_ARADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARLEN" *)
output wire [7 : 0] m_axi_MESSAGE_ARLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARSIZE" *)
output wire [2 : 0] m_axi_MESSAGE_ARSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARBURST" *)
output wire [1 : 0] m_axi_MESSAGE_ARBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARLOCK" *)
output wire [1 : 0] m_axi_MESSAGE_ARLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARREGION" *)
output wire [3 : 0] m_axi_MESSAGE_ARREGION;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARCACHE" *)
output wire [3 : 0] m_axi_MESSAGE_ARCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARPROT" *)
output wire [2 : 0] m_axi_MESSAGE_ARPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARQOS" *)
output wire [3 : 0] m_axi_MESSAGE_ARQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARVALID" *)
output wire m_axi_MESSAGE_ARVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE ARREADY" *)
input wire m_axi_MESSAGE_ARREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE RDATA" *)
input wire [31 : 0] m_axi_MESSAGE_RDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE RRESP" *)
input wire [1 : 0] m_axi_MESSAGE_RRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE RLAST" *)
input wire m_axi_MESSAGE_RLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE RVALID" *)
input wire m_axi_MESSAGE_RVALID;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axi_MESSAGE, ADDR_WIDTH 32, MAX_BURST_LENGTH 256, NUM_READ_OUTSTANDING 16, NUM_WRITE_OUTSTANDING 16, MAX_READ_BURST_LENGTH 16, MAX_WRITE_BURST_LENGTH 16, PROTOCOL AXI4, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 1, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, PHASE 0.000, CLK_DOMA\
IN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_MESSAGE RREADY" *)
output wire m_axi_MESSAGE_RREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWADDR" *)
output wire [31 : 0] m_axi_PARSED_DATA_AWADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWLEN" *)
output wire [7 : 0] m_axi_PARSED_DATA_AWLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWSIZE" *)
output wire [2 : 0] m_axi_PARSED_DATA_AWSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWBURST" *)
output wire [1 : 0] m_axi_PARSED_DATA_AWBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWLOCK" *)
output wire [1 : 0] m_axi_PARSED_DATA_AWLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWREGION" *)
output wire [3 : 0] m_axi_PARSED_DATA_AWREGION;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWCACHE" *)
output wire [3 : 0] m_axi_PARSED_DATA_AWCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWPROT" *)
output wire [2 : 0] m_axi_PARSED_DATA_AWPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWQOS" *)
output wire [3 : 0] m_axi_PARSED_DATA_AWQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWVALID" *)
output wire m_axi_PARSED_DATA_AWVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA AWREADY" *)
input wire m_axi_PARSED_DATA_AWREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA WDATA" *)
output wire [31 : 0] m_axi_PARSED_DATA_WDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA WSTRB" *)
output wire [3 : 0] m_axi_PARSED_DATA_WSTRB;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA WLAST" *)
output wire m_axi_PARSED_DATA_WLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA WVALID" *)
output wire m_axi_PARSED_DATA_WVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA WREADY" *)
input wire m_axi_PARSED_DATA_WREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA BRESP" *)
input wire [1 : 0] m_axi_PARSED_DATA_BRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA BVALID" *)
input wire m_axi_PARSED_DATA_BVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA BREADY" *)
output wire m_axi_PARSED_DATA_BREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARADDR" *)
output wire [31 : 0] m_axi_PARSED_DATA_ARADDR;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARLEN" *)
output wire [7 : 0] m_axi_PARSED_DATA_ARLEN;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARSIZE" *)
output wire [2 : 0] m_axi_PARSED_DATA_ARSIZE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARBURST" *)
output wire [1 : 0] m_axi_PARSED_DATA_ARBURST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARLOCK" *)
output wire [1 : 0] m_axi_PARSED_DATA_ARLOCK;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARREGION" *)
output wire [3 : 0] m_axi_PARSED_DATA_ARREGION;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARCACHE" *)
output wire [3 : 0] m_axi_PARSED_DATA_ARCACHE;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARPROT" *)
output wire [2 : 0] m_axi_PARSED_DATA_ARPROT;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARQOS" *)
output wire [3 : 0] m_axi_PARSED_DATA_ARQOS;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARVALID" *)
output wire m_axi_PARSED_DATA_ARVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA ARREADY" *)
input wire m_axi_PARSED_DATA_ARREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA RDATA" *)
input wire [31 : 0] m_axi_PARSED_DATA_RDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA RRESP" *)
input wire [1 : 0] m_axi_PARSED_DATA_RRESP;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA RLAST" *)
input wire m_axi_PARSED_DATA_RLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA RVALID" *)
input wire m_axi_PARSED_DATA_RVALID;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axi_PARSED_DATA, ADDR_WIDTH 32, MAX_BURST_LENGTH 256, NUM_READ_OUTSTANDING 16, NUM_WRITE_OUTSTANDING 16, MAX_READ_BURST_LENGTH 16, MAX_WRITE_BURST_LENGTH 16, PROTOCOL AXI4, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 1, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, PHASE 0.000, CLK_\
DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi_PARSED_DATA RREADY" *)
output wire m_axi_PARSED_DATA_RREADY;

  decode #(
    .C_S_AXI_CTL_ADDR_WIDTH(7),
    .C_S_AXI_CTL_DATA_WIDTH(32),
    .C_M_AXI_MESSAGE_ID_WIDTH(1),
    .C_M_AXI_MESSAGE_ADDR_WIDTH(32),
    .C_M_AXI_MESSAGE_DATA_WIDTH(32),
    .C_M_AXI_MESSAGE_AWUSER_WIDTH(1),
    .C_M_AXI_MESSAGE_ARUSER_WIDTH(1),
    .C_M_AXI_MESSAGE_WUSER_WIDTH(1),
    .C_M_AXI_MESSAGE_RUSER_WIDTH(1),
    .C_M_AXI_MESSAGE_BUSER_WIDTH(1),
    .C_M_AXI_MESSAGE_USER_VALUE('H00000000),
    .C_M_AXI_MESSAGE_PROT_VALUE('B000),
    .C_M_AXI_MESSAGE_CACHE_VALUE('B0011),
    .C_M_AXI_PARSED_DATA_ID_WIDTH(1),
    .C_M_AXI_PARSED_DATA_ADDR_WIDTH(32),
    .C_M_AXI_PARSED_DATA_DATA_WIDTH(32),
    .C_M_AXI_PARSED_DATA_AWUSER_WIDTH(1),
    .C_M_AXI_PARSED_DATA_ARUSER_WIDTH(1),
    .C_M_AXI_PARSED_DATA_WUSER_WIDTH(1),
    .C_M_AXI_PARSED_DATA_RUSER_WIDTH(1),
    .C_M_AXI_PARSED_DATA_BUSER_WIDTH(1),
    .C_M_AXI_PARSED_DATA_USER_VALUE('H00000000),
    .C_M_AXI_PARSED_DATA_PROT_VALUE('B000),
    .C_M_AXI_PARSED_DATA_CACHE_VALUE('B0011)
  ) inst (
    .s_axi_CTL_AWADDR(s_axi_CTL_AWADDR),
    .s_axi_CTL_AWVALID(s_axi_CTL_AWVALID),
    .s_axi_CTL_AWREADY(s_axi_CTL_AWREADY),
    .s_axi_CTL_WDATA(s_axi_CTL_WDATA),
    .s_axi_CTL_WSTRB(s_axi_CTL_WSTRB),
    .s_axi_CTL_WVALID(s_axi_CTL_WVALID),
    .s_axi_CTL_WREADY(s_axi_CTL_WREADY),
    .s_axi_CTL_BRESP(s_axi_CTL_BRESP),
    .s_axi_CTL_BVALID(s_axi_CTL_BVALID),
    .s_axi_CTL_BREADY(s_axi_CTL_BREADY),
    .s_axi_CTL_ARADDR(s_axi_CTL_ARADDR),
    .s_axi_CTL_ARVALID(s_axi_CTL_ARVALID),
    .s_axi_CTL_ARREADY(s_axi_CTL_ARREADY),
    .s_axi_CTL_RDATA(s_axi_CTL_RDATA),
    .s_axi_CTL_RRESP(s_axi_CTL_RRESP),
    .s_axi_CTL_RVALID(s_axi_CTL_RVALID),
    .s_axi_CTL_RREADY(s_axi_CTL_RREADY),
    .ap_clk(ap_clk),
    .ap_rst_n(ap_rst_n),
    .interrupt(interrupt),
    .m_axi_MESSAGE_AWID(),
    .m_axi_MESSAGE_AWADDR(m_axi_MESSAGE_AWADDR),
    .m_axi_MESSAGE_AWLEN(m_axi_MESSAGE_AWLEN),
    .m_axi_MESSAGE_AWSIZE(m_axi_MESSAGE_AWSIZE),
    .m_axi_MESSAGE_AWBURST(m_axi_MESSAGE_AWBURST),
    .m_axi_MESSAGE_AWLOCK(m_axi_MESSAGE_AWLOCK),
    .m_axi_MESSAGE_AWREGION(m_axi_MESSAGE_AWREGION),
    .m_axi_MESSAGE_AWCACHE(m_axi_MESSAGE_AWCACHE),
    .m_axi_MESSAGE_AWPROT(m_axi_MESSAGE_AWPROT),
    .m_axi_MESSAGE_AWQOS(m_axi_MESSAGE_AWQOS),
    .m_axi_MESSAGE_AWUSER(),
    .m_axi_MESSAGE_AWVALID(m_axi_MESSAGE_AWVALID),
    .m_axi_MESSAGE_AWREADY(m_axi_MESSAGE_AWREADY),
    .m_axi_MESSAGE_WID(),
    .m_axi_MESSAGE_WDATA(m_axi_MESSAGE_WDATA),
    .m_axi_MESSAGE_WSTRB(m_axi_MESSAGE_WSTRB),
    .m_axi_MESSAGE_WLAST(m_axi_MESSAGE_WLAST),
    .m_axi_MESSAGE_WUSER(),
    .m_axi_MESSAGE_WVALID(m_axi_MESSAGE_WVALID),
    .m_axi_MESSAGE_WREADY(m_axi_MESSAGE_WREADY),
    .m_axi_MESSAGE_BID(1'B0),
    .m_axi_MESSAGE_BRESP(m_axi_MESSAGE_BRESP),
    .m_axi_MESSAGE_BUSER(1'B0),
    .m_axi_MESSAGE_BVALID(m_axi_MESSAGE_BVALID),
    .m_axi_MESSAGE_BREADY(m_axi_MESSAGE_BREADY),
    .m_axi_MESSAGE_ARID(),
    .m_axi_MESSAGE_ARADDR(m_axi_MESSAGE_ARADDR),
    .m_axi_MESSAGE_ARLEN(m_axi_MESSAGE_ARLEN),
    .m_axi_MESSAGE_ARSIZE(m_axi_MESSAGE_ARSIZE),
    .m_axi_MESSAGE_ARBURST(m_axi_MESSAGE_ARBURST),
    .m_axi_MESSAGE_ARLOCK(m_axi_MESSAGE_ARLOCK),
    .m_axi_MESSAGE_ARREGION(m_axi_MESSAGE_ARREGION),
    .m_axi_MESSAGE_ARCACHE(m_axi_MESSAGE_ARCACHE),
    .m_axi_MESSAGE_ARPROT(m_axi_MESSAGE_ARPROT),
    .m_axi_MESSAGE_ARQOS(m_axi_MESSAGE_ARQOS),
    .m_axi_MESSAGE_ARUSER(),
    .m_axi_MESSAGE_ARVALID(m_axi_MESSAGE_ARVALID),
    .m_axi_MESSAGE_ARREADY(m_axi_MESSAGE_ARREADY),
    .m_axi_MESSAGE_RID(1'B0),
    .m_axi_MESSAGE_RDATA(m_axi_MESSAGE_RDATA),
    .m_axi_MESSAGE_RRESP(m_axi_MESSAGE_RRESP),
    .m_axi_MESSAGE_RLAST(m_axi_MESSAGE_RLAST),
    .m_axi_MESSAGE_RUSER(1'B0),
    .m_axi_MESSAGE_RVALID(m_axi_MESSAGE_RVALID),
    .m_axi_MESSAGE_RREADY(m_axi_MESSAGE_RREADY),
    .m_axi_PARSED_DATA_AWID(),
    .m_axi_PARSED_DATA_AWADDR(m_axi_PARSED_DATA_AWADDR),
    .m_axi_PARSED_DATA_AWLEN(m_axi_PARSED_DATA_AWLEN),
    .m_axi_PARSED_DATA_AWSIZE(m_axi_PARSED_DATA_AWSIZE),
    .m_axi_PARSED_DATA_AWBURST(m_axi_PARSED_DATA_AWBURST),
    .m_axi_PARSED_DATA_AWLOCK(m_axi_PARSED_DATA_AWLOCK),
    .m_axi_PARSED_DATA_AWREGION(m_axi_PARSED_DATA_AWREGION),
    .m_axi_PARSED_DATA_AWCACHE(m_axi_PARSED_DATA_AWCACHE),
    .m_axi_PARSED_DATA_AWPROT(m_axi_PARSED_DATA_AWPROT),
    .m_axi_PARSED_DATA_AWQOS(m_axi_PARSED_DATA_AWQOS),
    .m_axi_PARSED_DATA_AWUSER(),
    .m_axi_PARSED_DATA_AWVALID(m_axi_PARSED_DATA_AWVALID),
    .m_axi_PARSED_DATA_AWREADY(m_axi_PARSED_DATA_AWREADY),
    .m_axi_PARSED_DATA_WID(),
    .m_axi_PARSED_DATA_WDATA(m_axi_PARSED_DATA_WDATA),
    .m_axi_PARSED_DATA_WSTRB(m_axi_PARSED_DATA_WSTRB),
    .m_axi_PARSED_DATA_WLAST(m_axi_PARSED_DATA_WLAST),
    .m_axi_PARSED_DATA_WUSER(),
    .m_axi_PARSED_DATA_WVALID(m_axi_PARSED_DATA_WVALID),
    .m_axi_PARSED_DATA_WREADY(m_axi_PARSED_DATA_WREADY),
    .m_axi_PARSED_DATA_BID(1'B0),
    .m_axi_PARSED_DATA_BRESP(m_axi_PARSED_DATA_BRESP),
    .m_axi_PARSED_DATA_BUSER(1'B0),
    .m_axi_PARSED_DATA_BVALID(m_axi_PARSED_DATA_BVALID),
    .m_axi_PARSED_DATA_BREADY(m_axi_PARSED_DATA_BREADY),
    .m_axi_PARSED_DATA_ARID(),
    .m_axi_PARSED_DATA_ARADDR(m_axi_PARSED_DATA_ARADDR),
    .m_axi_PARSED_DATA_ARLEN(m_axi_PARSED_DATA_ARLEN),
    .m_axi_PARSED_DATA_ARSIZE(m_axi_PARSED_DATA_ARSIZE),
    .m_axi_PARSED_DATA_ARBURST(m_axi_PARSED_DATA_ARBURST),
    .m_axi_PARSED_DATA_ARLOCK(m_axi_PARSED_DATA_ARLOCK),
    .m_axi_PARSED_DATA_ARREGION(m_axi_PARSED_DATA_ARREGION),
    .m_axi_PARSED_DATA_ARCACHE(m_axi_PARSED_DATA_ARCACHE),
    .m_axi_PARSED_DATA_ARPROT(m_axi_PARSED_DATA_ARPROT),
    .m_axi_PARSED_DATA_ARQOS(m_axi_PARSED_DATA_ARQOS),
    .m_axi_PARSED_DATA_ARUSER(),
    .m_axi_PARSED_DATA_ARVALID(m_axi_PARSED_DATA_ARVALID),
    .m_axi_PARSED_DATA_ARREADY(m_axi_PARSED_DATA_ARREADY),
    .m_axi_PARSED_DATA_RID(1'B0),
    .m_axi_PARSED_DATA_RDATA(m_axi_PARSED_DATA_RDATA),
    .m_axi_PARSED_DATA_RRESP(m_axi_PARSED_DATA_RRESP),
    .m_axi_PARSED_DATA_RLAST(m_axi_PARSED_DATA_RLAST),
    .m_axi_PARSED_DATA_RUSER(1'B0),
    .m_axi_PARSED_DATA_RVALID(m_axi_PARSED_DATA_RVALID),
    .m_axi_PARSED_DATA_RREADY(m_axi_PARSED_DATA_RREADY)
  );
endmodule
