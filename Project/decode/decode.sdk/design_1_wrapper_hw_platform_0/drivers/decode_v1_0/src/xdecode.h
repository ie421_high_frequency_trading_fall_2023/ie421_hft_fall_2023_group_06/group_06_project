// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XDECODE_H
#define XDECODE_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xdecode_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Ctl_BaseAddress;
} XDecode_Config;
#endif

typedef struct {
    u32 Ctl_BaseAddress;
    u32 IsReady;
} XDecode;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XDecode_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XDecode_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XDecode_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XDecode_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XDecode_Initialize(XDecode *InstancePtr, u16 DeviceId);
XDecode_Config* XDecode_LookupConfig(u16 DeviceId);
int XDecode_CfgInitialize(XDecode *InstancePtr, XDecode_Config *ConfigPtr);
#else
int XDecode_Initialize(XDecode *InstancePtr, const char* InstanceName);
int XDecode_Release(XDecode *InstancePtr);
#endif

void XDecode_Start(XDecode *InstancePtr);
u32 XDecode_IsDone(XDecode *InstancePtr);
u32 XDecode_IsIdle(XDecode *InstancePtr);
u32 XDecode_IsReady(XDecode *InstancePtr);
void XDecode_EnableAutoRestart(XDecode *InstancePtr);
void XDecode_DisableAutoRestart(XDecode *InstancePtr);
u32 XDecode_Get_return(XDecode *InstancePtr);

void XDecode_Set_message_payload(XDecode *InstancePtr, u32 Data);
u32 XDecode_Get_message_payload(XDecode *InstancePtr);
void XDecode_Set_size(XDecode *InstancePtr, u32 Data);
u32 XDecode_Get_size(XDecode *InstancePtr);
void XDecode_Set_symbol(XDecode *InstancePtr, u32 Data);
u32 XDecode_Get_symbol(XDecode *InstancePtr);
void XDecode_Set_price(XDecode *InstancePtr, u32 Data);
u32 XDecode_Get_price(XDecode *InstancePtr);
void XDecode_Set_sell_or_buy(XDecode *InstancePtr, u32 Data);
u32 XDecode_Get_sell_or_buy(XDecode *InstancePtr);
void XDecode_Set_is_system_event(XDecode *InstancePtr, u32 Data);
u32 XDecode_Get_is_system_event(XDecode *InstancePtr);
void XDecode_Set_system_event_message(XDecode *InstancePtr, u32 Data);
u32 XDecode_Get_system_event_message(XDecode *InstancePtr);

void XDecode_InterruptGlobalEnable(XDecode *InstancePtr);
void XDecode_InterruptGlobalDisable(XDecode *InstancePtr);
void XDecode_InterruptEnable(XDecode *InstancePtr, u32 Mask);
void XDecode_InterruptDisable(XDecode *InstancePtr, u32 Mask);
void XDecode_InterruptClear(XDecode *InstancePtr, u32 Mask);
u32 XDecode_InterruptGetEnabled(XDecode *InstancePtr);
u32 XDecode_InterruptGetStatus(XDecode *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
