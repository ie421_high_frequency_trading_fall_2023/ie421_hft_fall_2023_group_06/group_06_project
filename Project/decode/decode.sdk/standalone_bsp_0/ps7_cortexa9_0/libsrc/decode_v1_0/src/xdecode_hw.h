// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// CTL
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read/COR)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - Channel 0 (ap_done)
//        bit 1  - Channel 1 (ap_ready)
//        others - reserved
// 0x10 : Data signal of ap_return
//        bit 31~0 - ap_return[31:0] (Read)
// 0x18 : Data signal of message_payload
//        bit 31~0 - message_payload[31:0] (Read/Write)
// 0x1c : reserved
// 0x20 : Data signal of size
//        bit 31~0 - size[31:0] (Read/Write)
// 0x24 : reserved
// 0x28 : Data signal of symbol
//        bit 31~0 - symbol[31:0] (Read/Write)
// 0x2c : reserved
// 0x30 : Data signal of price
//        bit 31~0 - price[31:0] (Read/Write)
// 0x34 : reserved
// 0x38 : Data signal of sell_or_buy
//        bit 31~0 - sell_or_buy[31:0] (Read/Write)
// 0x3c : reserved
// 0x40 : Data signal of is_system_event
//        bit 31~0 - is_system_event[31:0] (Read/Write)
// 0x44 : reserved
// 0x48 : Data signal of system_event_message
//        bit 31~0 - system_event_message[31:0] (Read/Write)
// 0x4c : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XDECODE_CTL_ADDR_AP_CTRL                   0x00
#define XDECODE_CTL_ADDR_GIE                       0x04
#define XDECODE_CTL_ADDR_IER                       0x08
#define XDECODE_CTL_ADDR_ISR                       0x0c
#define XDECODE_CTL_ADDR_AP_RETURN                 0x10
#define XDECODE_CTL_BITS_AP_RETURN                 32
#define XDECODE_CTL_ADDR_MESSAGE_PAYLOAD_DATA      0x18
#define XDECODE_CTL_BITS_MESSAGE_PAYLOAD_DATA      32
#define XDECODE_CTL_ADDR_SIZE_DATA                 0x20
#define XDECODE_CTL_BITS_SIZE_DATA                 32
#define XDECODE_CTL_ADDR_SYMBOL_DATA               0x28
#define XDECODE_CTL_BITS_SYMBOL_DATA               32
#define XDECODE_CTL_ADDR_PRICE_DATA                0x30
#define XDECODE_CTL_BITS_PRICE_DATA                32
#define XDECODE_CTL_ADDR_SELL_OR_BUY_DATA          0x38
#define XDECODE_CTL_BITS_SELL_OR_BUY_DATA          32
#define XDECODE_CTL_ADDR_IS_SYSTEM_EVENT_DATA      0x40
#define XDECODE_CTL_BITS_IS_SYSTEM_EVENT_DATA      32
#define XDECODE_CTL_ADDR_SYSTEM_EVENT_MESSAGE_DATA 0x48
#define XDECODE_CTL_BITS_SYSTEM_EVENT_MESSAGE_DATA 32

