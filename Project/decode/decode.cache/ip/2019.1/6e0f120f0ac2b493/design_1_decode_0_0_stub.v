// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Dec 14 10:40:56 2023
// Host        : jianjian running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_decode_0_0_stub.v
// Design      : design_1_decode_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "decode,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(s_axi_CTL_AWADDR, s_axi_CTL_AWVALID, 
  s_axi_CTL_AWREADY, s_axi_CTL_WDATA, s_axi_CTL_WSTRB, s_axi_CTL_WVALID, s_axi_CTL_WREADY, 
  s_axi_CTL_BRESP, s_axi_CTL_BVALID, s_axi_CTL_BREADY, s_axi_CTL_ARADDR, s_axi_CTL_ARVALID, 
  s_axi_CTL_ARREADY, s_axi_CTL_RDATA, s_axi_CTL_RRESP, s_axi_CTL_RVALID, s_axi_CTL_RREADY, 
  ap_clk, ap_rst_n, interrupt, m_axi_MESSAGE_AWADDR, m_axi_MESSAGE_AWLEN, 
  m_axi_MESSAGE_AWSIZE, m_axi_MESSAGE_AWBURST, m_axi_MESSAGE_AWLOCK, 
  m_axi_MESSAGE_AWREGION, m_axi_MESSAGE_AWCACHE, m_axi_MESSAGE_AWPROT, 
  m_axi_MESSAGE_AWQOS, m_axi_MESSAGE_AWVALID, m_axi_MESSAGE_AWREADY, m_axi_MESSAGE_WDATA, 
  m_axi_MESSAGE_WSTRB, m_axi_MESSAGE_WLAST, m_axi_MESSAGE_WVALID, m_axi_MESSAGE_WREADY, 
  m_axi_MESSAGE_BRESP, m_axi_MESSAGE_BVALID, m_axi_MESSAGE_BREADY, m_axi_MESSAGE_ARADDR, 
  m_axi_MESSAGE_ARLEN, m_axi_MESSAGE_ARSIZE, m_axi_MESSAGE_ARBURST, m_axi_MESSAGE_ARLOCK, 
  m_axi_MESSAGE_ARREGION, m_axi_MESSAGE_ARCACHE, m_axi_MESSAGE_ARPROT, 
  m_axi_MESSAGE_ARQOS, m_axi_MESSAGE_ARVALID, m_axi_MESSAGE_ARREADY, m_axi_MESSAGE_RDATA, 
  m_axi_MESSAGE_RRESP, m_axi_MESSAGE_RLAST, m_axi_MESSAGE_RVALID, m_axi_MESSAGE_RREADY, 
  m_axi_PARSED_DATA_AWADDR, m_axi_PARSED_DATA_AWLEN, m_axi_PARSED_DATA_AWSIZE, 
  m_axi_PARSED_DATA_AWBURST, m_axi_PARSED_DATA_AWLOCK, m_axi_PARSED_DATA_AWREGION, 
  m_axi_PARSED_DATA_AWCACHE, m_axi_PARSED_DATA_AWPROT, m_axi_PARSED_DATA_AWQOS, 
  m_axi_PARSED_DATA_AWVALID, m_axi_PARSED_DATA_AWREADY, m_axi_PARSED_DATA_WDATA, 
  m_axi_PARSED_DATA_WSTRB, m_axi_PARSED_DATA_WLAST, m_axi_PARSED_DATA_WVALID, 
  m_axi_PARSED_DATA_WREADY, m_axi_PARSED_DATA_BRESP, m_axi_PARSED_DATA_BVALID, 
  m_axi_PARSED_DATA_BREADY, m_axi_PARSED_DATA_ARADDR, m_axi_PARSED_DATA_ARLEN, 
  m_axi_PARSED_DATA_ARSIZE, m_axi_PARSED_DATA_ARBURST, m_axi_PARSED_DATA_ARLOCK, 
  m_axi_PARSED_DATA_ARREGION, m_axi_PARSED_DATA_ARCACHE, m_axi_PARSED_DATA_ARPROT, 
  m_axi_PARSED_DATA_ARQOS, m_axi_PARSED_DATA_ARVALID, m_axi_PARSED_DATA_ARREADY, 
  m_axi_PARSED_DATA_RDATA, m_axi_PARSED_DATA_RRESP, m_axi_PARSED_DATA_RLAST, 
  m_axi_PARSED_DATA_RVALID, m_axi_PARSED_DATA_RREADY)
/* synthesis syn_black_box black_box_pad_pin="s_axi_CTL_AWADDR[6:0],s_axi_CTL_AWVALID,s_axi_CTL_AWREADY,s_axi_CTL_WDATA[31:0],s_axi_CTL_WSTRB[3:0],s_axi_CTL_WVALID,s_axi_CTL_WREADY,s_axi_CTL_BRESP[1:0],s_axi_CTL_BVALID,s_axi_CTL_BREADY,s_axi_CTL_ARADDR[6:0],s_axi_CTL_ARVALID,s_axi_CTL_ARREADY,s_axi_CTL_RDATA[31:0],s_axi_CTL_RRESP[1:0],s_axi_CTL_RVALID,s_axi_CTL_RREADY,ap_clk,ap_rst_n,interrupt,m_axi_MESSAGE_AWADDR[31:0],m_axi_MESSAGE_AWLEN[7:0],m_axi_MESSAGE_AWSIZE[2:0],m_axi_MESSAGE_AWBURST[1:0],m_axi_MESSAGE_AWLOCK[1:0],m_axi_MESSAGE_AWREGION[3:0],m_axi_MESSAGE_AWCACHE[3:0],m_axi_MESSAGE_AWPROT[2:0],m_axi_MESSAGE_AWQOS[3:0],m_axi_MESSAGE_AWVALID,m_axi_MESSAGE_AWREADY,m_axi_MESSAGE_WDATA[31:0],m_axi_MESSAGE_WSTRB[3:0],m_axi_MESSAGE_WLAST,m_axi_MESSAGE_WVALID,m_axi_MESSAGE_WREADY,m_axi_MESSAGE_BRESP[1:0],m_axi_MESSAGE_BVALID,m_axi_MESSAGE_BREADY,m_axi_MESSAGE_ARADDR[31:0],m_axi_MESSAGE_ARLEN[7:0],m_axi_MESSAGE_ARSIZE[2:0],m_axi_MESSAGE_ARBURST[1:0],m_axi_MESSAGE_ARLOCK[1:0],m_axi_MESSAGE_ARREGION[3:0],m_axi_MESSAGE_ARCACHE[3:0],m_axi_MESSAGE_ARPROT[2:0],m_axi_MESSAGE_ARQOS[3:0],m_axi_MESSAGE_ARVALID,m_axi_MESSAGE_ARREADY,m_axi_MESSAGE_RDATA[31:0],m_axi_MESSAGE_RRESP[1:0],m_axi_MESSAGE_RLAST,m_axi_MESSAGE_RVALID,m_axi_MESSAGE_RREADY,m_axi_PARSED_DATA_AWADDR[31:0],m_axi_PARSED_DATA_AWLEN[7:0],m_axi_PARSED_DATA_AWSIZE[2:0],m_axi_PARSED_DATA_AWBURST[1:0],m_axi_PARSED_DATA_AWLOCK[1:0],m_axi_PARSED_DATA_AWREGION[3:0],m_axi_PARSED_DATA_AWCACHE[3:0],m_axi_PARSED_DATA_AWPROT[2:0],m_axi_PARSED_DATA_AWQOS[3:0],m_axi_PARSED_DATA_AWVALID,m_axi_PARSED_DATA_AWREADY,m_axi_PARSED_DATA_WDATA[31:0],m_axi_PARSED_DATA_WSTRB[3:0],m_axi_PARSED_DATA_WLAST,m_axi_PARSED_DATA_WVALID,m_axi_PARSED_DATA_WREADY,m_axi_PARSED_DATA_BRESP[1:0],m_axi_PARSED_DATA_BVALID,m_axi_PARSED_DATA_BREADY,m_axi_PARSED_DATA_ARADDR[31:0],m_axi_PARSED_DATA_ARLEN[7:0],m_axi_PARSED_DATA_ARSIZE[2:0],m_axi_PARSED_DATA_ARBURST[1:0],m_axi_PARSED_DATA_ARLOCK[1:0],m_axi_PARSED_DATA_ARREGION[3:0],m_axi_PARSED_DATA_ARCACHE[3:0],m_axi_PARSED_DATA_ARPROT[2:0],m_axi_PARSED_DATA_ARQOS[3:0],m_axi_PARSED_DATA_ARVALID,m_axi_PARSED_DATA_ARREADY,m_axi_PARSED_DATA_RDATA[31:0],m_axi_PARSED_DATA_RRESP[1:0],m_axi_PARSED_DATA_RLAST,m_axi_PARSED_DATA_RVALID,m_axi_PARSED_DATA_RREADY" */;
  input [6:0]s_axi_CTL_AWADDR;
  input s_axi_CTL_AWVALID;
  output s_axi_CTL_AWREADY;
  input [31:0]s_axi_CTL_WDATA;
  input [3:0]s_axi_CTL_WSTRB;
  input s_axi_CTL_WVALID;
  output s_axi_CTL_WREADY;
  output [1:0]s_axi_CTL_BRESP;
  output s_axi_CTL_BVALID;
  input s_axi_CTL_BREADY;
  input [6:0]s_axi_CTL_ARADDR;
  input s_axi_CTL_ARVALID;
  output s_axi_CTL_ARREADY;
  output [31:0]s_axi_CTL_RDATA;
  output [1:0]s_axi_CTL_RRESP;
  output s_axi_CTL_RVALID;
  input s_axi_CTL_RREADY;
  input ap_clk;
  input ap_rst_n;
  output interrupt;
  output [31:0]m_axi_MESSAGE_AWADDR;
  output [7:0]m_axi_MESSAGE_AWLEN;
  output [2:0]m_axi_MESSAGE_AWSIZE;
  output [1:0]m_axi_MESSAGE_AWBURST;
  output [1:0]m_axi_MESSAGE_AWLOCK;
  output [3:0]m_axi_MESSAGE_AWREGION;
  output [3:0]m_axi_MESSAGE_AWCACHE;
  output [2:0]m_axi_MESSAGE_AWPROT;
  output [3:0]m_axi_MESSAGE_AWQOS;
  output m_axi_MESSAGE_AWVALID;
  input m_axi_MESSAGE_AWREADY;
  output [31:0]m_axi_MESSAGE_WDATA;
  output [3:0]m_axi_MESSAGE_WSTRB;
  output m_axi_MESSAGE_WLAST;
  output m_axi_MESSAGE_WVALID;
  input m_axi_MESSAGE_WREADY;
  input [1:0]m_axi_MESSAGE_BRESP;
  input m_axi_MESSAGE_BVALID;
  output m_axi_MESSAGE_BREADY;
  output [31:0]m_axi_MESSAGE_ARADDR;
  output [7:0]m_axi_MESSAGE_ARLEN;
  output [2:0]m_axi_MESSAGE_ARSIZE;
  output [1:0]m_axi_MESSAGE_ARBURST;
  output [1:0]m_axi_MESSAGE_ARLOCK;
  output [3:0]m_axi_MESSAGE_ARREGION;
  output [3:0]m_axi_MESSAGE_ARCACHE;
  output [2:0]m_axi_MESSAGE_ARPROT;
  output [3:0]m_axi_MESSAGE_ARQOS;
  output m_axi_MESSAGE_ARVALID;
  input m_axi_MESSAGE_ARREADY;
  input [31:0]m_axi_MESSAGE_RDATA;
  input [1:0]m_axi_MESSAGE_RRESP;
  input m_axi_MESSAGE_RLAST;
  input m_axi_MESSAGE_RVALID;
  output m_axi_MESSAGE_RREADY;
  output [31:0]m_axi_PARSED_DATA_AWADDR;
  output [7:0]m_axi_PARSED_DATA_AWLEN;
  output [2:0]m_axi_PARSED_DATA_AWSIZE;
  output [1:0]m_axi_PARSED_DATA_AWBURST;
  output [1:0]m_axi_PARSED_DATA_AWLOCK;
  output [3:0]m_axi_PARSED_DATA_AWREGION;
  output [3:0]m_axi_PARSED_DATA_AWCACHE;
  output [2:0]m_axi_PARSED_DATA_AWPROT;
  output [3:0]m_axi_PARSED_DATA_AWQOS;
  output m_axi_PARSED_DATA_AWVALID;
  input m_axi_PARSED_DATA_AWREADY;
  output [31:0]m_axi_PARSED_DATA_WDATA;
  output [3:0]m_axi_PARSED_DATA_WSTRB;
  output m_axi_PARSED_DATA_WLAST;
  output m_axi_PARSED_DATA_WVALID;
  input m_axi_PARSED_DATA_WREADY;
  input [1:0]m_axi_PARSED_DATA_BRESP;
  input m_axi_PARSED_DATA_BVALID;
  output m_axi_PARSED_DATA_BREADY;
  output [31:0]m_axi_PARSED_DATA_ARADDR;
  output [7:0]m_axi_PARSED_DATA_ARLEN;
  output [2:0]m_axi_PARSED_DATA_ARSIZE;
  output [1:0]m_axi_PARSED_DATA_ARBURST;
  output [1:0]m_axi_PARSED_DATA_ARLOCK;
  output [3:0]m_axi_PARSED_DATA_ARREGION;
  output [3:0]m_axi_PARSED_DATA_ARCACHE;
  output [2:0]m_axi_PARSED_DATA_ARPROT;
  output [3:0]m_axi_PARSED_DATA_ARQOS;
  output m_axi_PARSED_DATA_ARVALID;
  input m_axi_PARSED_DATA_ARREADY;
  input [31:0]m_axi_PARSED_DATA_RDATA;
  input [1:0]m_axi_PARSED_DATA_RRESP;
  input m_axi_PARSED_DATA_RLAST;
  input m_axi_PARSED_DATA_RVALID;
  output m_axi_PARSED_DATA_RREADY;
endmodule
