// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xdecode.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XDecode_CfgInitialize(XDecode *InstancePtr, XDecode_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Ctl_BaseAddress = ConfigPtr->Ctl_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XDecode_Start(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_CTRL) & 0x80;
    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_CTRL, Data | 0x01);
}

u32 XDecode_IsDone(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XDecode_IsIdle(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XDecode_IsReady(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XDecode_EnableAutoRestart(XDecode *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_CTRL, 0x80);
}

void XDecode_DisableAutoRestart(XDecode *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_CTRL, 0);
}

u32 XDecode_Get_return(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_AP_RETURN);
    return Data;
}
void XDecode_Set_message_payload(XDecode *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_MESSAGE_PAYLOAD_DATA, Data);
}

u32 XDecode_Get_message_payload(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_MESSAGE_PAYLOAD_DATA);
    return Data;
}

void XDecode_Set_size(XDecode *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SIZE_DATA, Data);
}

u32 XDecode_Get_size(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SIZE_DATA);
    return Data;
}

void XDecode_Set_symbol(XDecode *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SYMBOL_DATA, Data);
}

u32 XDecode_Get_symbol(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SYMBOL_DATA);
    return Data;
}

void XDecode_Set_price(XDecode *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_PRICE_DATA, Data);
}

u32 XDecode_Get_price(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_PRICE_DATA);
    return Data;
}

void XDecode_Set_sell_or_buy(XDecode *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SELL_OR_BUY_DATA, Data);
}

u32 XDecode_Get_sell_or_buy(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SELL_OR_BUY_DATA);
    return Data;
}

void XDecode_Set_is_system_event(XDecode *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_IS_SYSTEM_EVENT_DATA, Data);
}

u32 XDecode_Get_is_system_event(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_IS_SYSTEM_EVENT_DATA);
    return Data;
}

void XDecode_Set_system_event_message(XDecode *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SYSTEM_EVENT_MESSAGE_DATA, Data);
}

u32 XDecode_Get_system_event_message(XDecode *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_SYSTEM_EVENT_MESSAGE_DATA);
    return Data;
}

void XDecode_InterruptGlobalEnable(XDecode *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_GIE, 1);
}

void XDecode_InterruptGlobalDisable(XDecode *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_GIE, 0);
}

void XDecode_InterruptEnable(XDecode *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_IER);
    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_IER, Register | Mask);
}

void XDecode_InterruptDisable(XDecode *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_IER);
    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_IER, Register & (~Mask));
}

void XDecode_InterruptClear(XDecode *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XDecode_WriteReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_ISR, Mask);
}

u32 XDecode_InterruptGetEnabled(XDecode *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_IER);
}

u32 XDecode_InterruptGetStatus(XDecode *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XDecode_ReadReg(InstancePtr->Ctl_BaseAddress, XDECODE_CTL_ADDR_ISR);
}

