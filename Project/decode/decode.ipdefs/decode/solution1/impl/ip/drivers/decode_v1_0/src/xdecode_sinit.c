// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xdecode.h"

extern XDecode_Config XDecode_ConfigTable[];

XDecode_Config *XDecode_LookupConfig(u16 DeviceId) {
	XDecode_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XDECODE_NUM_INSTANCES; Index++) {
		if (XDecode_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XDecode_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XDecode_Initialize(XDecode *InstancePtr, u16 DeviceId) {
	XDecode_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XDecode_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XDecode_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

