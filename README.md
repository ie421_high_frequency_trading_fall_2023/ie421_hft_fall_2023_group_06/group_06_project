# Group 6 IE High Frequency Trading Technology Git Repository  
This is Group 6 IE High Frequency Trading Technology Git Repository 

## Team Members

**Haijian Wang**
- I am a M.S student in ECE at the University of Illinois at Urbana-Champaign. I mainly focus on computer architecture, AI/ML, and HW/SW codesign. I also obtained a physics minor with emphasise on quantum.
- My email is haijian4@illinois.edu. Don't hesitate to email me if you have any questions!

**Jack Prokop (jprokop2@illinois.edu)**
- I am a current undergraduate student studying Computer Engineering with a graduation date in May 2025. I have taken courses in systems programming, parallel programming, digital systems, and algorithms. I am interested in embedded software and have skills including C/C++, CUDA, SystemVerilog, and Python.


**Sudheep Iyer**
- I am a current ECE major undergrad student with a minor in semiconductor engineering. I have both software and hardware experience with C/C++, Python, FPGAs, and PCB design.
- My Illinois email is sudheep2@illinois.edu.
## License

[MIT](https://choosealicense.com/licenses/mit/)
